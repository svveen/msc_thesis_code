import time
import json as js
import os
import bz2
import shutil

import pandas as pd
import requests

import config
import global_variables as gv


# Get AS relation and customer cone data from CAIDA for specific month
def get_AS_relationship_data(date):
    """Get AS relation and customer cone data from CAIDA for specific month

    Parameters:
        date (string): date of file in format YYYYMM01
    """

    directory = config.data_directory.joinpath('AS_relationship_data')

    # URLs to CAIDA data
    rel1_files = 'http://data.caida.org/datasets/as-relationships/serial-1/'
    rel2_files = 'http://data.caida.org/datasets/as-relationships/serial-2/'

    # Full URLs to files for specified month
    rel1 = rel1_files + date + '.as-rel.txt.bz2'  # AS relationships 1
    rel2 = rel2_files + date + '.as-rel2.txt.bz2'  # AS relationships 2
    ppdc = rel1_files + date + '.ppdc-ases.txt.bz2'  # Customer cones
    urls = [rel1, rel2, ppdc]

    # Download, extract and save files to AS_relationship_data directory
    rel1 = directory.joinpath(date + '.as-rel.txt.bz2')
    rel2 = directory.joinpath(date + '.as-rel2.txt.bz2')
    ppdc = directory.joinpath(date + '.ppdc-ases.txt.bz2')
    file_names = [rel1, rel2, ppdc]

    print('[+] Downloading CAIDA Relationship data for ' + date)
    for i in range(0, 3):
        r = requests.get(urls[i])  # Get data
        file = file_names[i]  # Get filename
        open(file, 'wb').write(r.content)  # Save compressed data
        with bz2.open(file, 'rb') as f_in:  # Decompress
            with open(file.as_posix().replace('.bz2', ''), 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        f_in.close()
        os.remove(file)  # Delete compressed file


# Loads AS relationship data from CAIDA files
def load_AS_relationship_data():
    """Load AS relationship data from CAIDA files.

    Returns:
        dict: dictionary of dataframes containing AS relationship data.
              Dictionary keys are the file names.
    """

    path = config.data_directory.joinpath('AS_relationship_data')
    file_names = os.listdir(path)
    relationship_data = dict()

    print('[+] Loading CAIDA relationship data')
    for i in file_names:
        file = path.joinpath(i)
        if i.endswith('as-rel.txt'):  # Relation 1 files
            rel1 = pd.read_csv(file, sep='|', header=None,
                               names=['AS_x', 'AS_y', 'relationship'],
                               comment='#')
            relationship_data[i] = rel1
        elif i.endswith('as-rel2.txt'):  # Relation 2 files
            rel2 = pd.read_csv(file, sep='|', header=None,
                               names=['AS_x', 'AS_y', 'relationship',
                                      'source'],
                               comment='#')
            relationship_data[i] = rel2
        elif i.endswith('ppdc-ases.txt'):  # Customer cone files
            ppdc = []
            with open(file) as inputfile:
                for line in inputfile:
                    if not line.startswith('#'):  # Ignore comments
                        # Put all customers in list
                        ppdc.append(line.split(' ', 1))
            ppdc = pd.DataFrame.from_records(
                    ppdc, columns=['cone_AS', 'customers'])
            relationship_data[i] = ppdc

    return relationship_data


# Get CAIDA organization data
def load_AS_organization_data():
    """Load AS organization data from CAIDA files.

    Returns:
        dict: dictionary of dataframes containing AS relationship data.
              Dictionary keys are the file names.
    """

    path = config.data_directory.joinpath('AS_organization_data')
    file_names = os.listdir(path)
    organization_data = dict()

    print('[+] Loading CAIDA organization data')
    for i in file_names:
        file = path.joinpath(i)
        file_data = pd.read_csv(file, sep='|', header=None,
                                names=['aut/org_id', 'changed', 'name',
                                       'org_id/country', 'source'],
                                comment='#')
        organization_data[i] = file_data
    return organization_data


# Load BGPStream events from files
def load_BGPStream_events():
    """Load BGPStream events from files

    Returns:
        list: list containing dictionary of events for each event type. List is
              ordered as follows: hijacks, route leaks, ASN outages, country
              outages.
    """
    hijacks = dict()
    route_leaks = dict()
    ASN_outages = dict()
    country_outages = dict()

    path = config.data_directory.joinpath('events')
    event_files = os.listdir(path)  # filenames of event files

    print('[+] Loading BGPStream events')
    for event in event_files:
        file_name = path.joinpath(event)
        with open(file_name) as json_data:
            event_data = js.load(json_data)
            if event_data['type'] == 'hijack':
                hijacks[event_data['event']] = event_data
            elif event_data['type'] == 'leak':
                route_leaks[event_data['event']] = event_data
            elif event_data['type'] == 'outage ASN':
                ASN_outages[event_data['event']] = event_data
            elif event_data['type'] == 'outage country':
                country_outages[event_data['event']] = event_data
    return [hijacks, route_leaks, ASN_outages, country_outages]


# Get AS history from RIPEstat between history start and history end
def get_AS_routing_history(asn):
    """Get AS routing history for specified AS between history start and end.

    Download AS history from RIPEstat for specified AS
    between history start and history end.

    Parameters:
        asn (int): AS number

    Returns:
        list: list containing AS history in json format. If AS history is
              too large to download in one piece, this list will contain
              the history split in 15 pieces.
    """

    path = config.data_directory.joinpath('AS_history')
    file_name = path.joinpath(str(asn) + '.txt')
    history_start = gv.history_boundaries[0]
    history_end = gv.history_boundaries[2]

    # Check if AS history is already downloaded in one piece
    if os.path.isfile(file_name):
        with open(file_name) as file_data:
            r = js.load(file_data)
        return [r]

    # Check if AS history is already downloaded in multiple pieces
    all_pieces = []
    found_files = False
    for i in range(1, 16):
        file_name = path.joinpath(str(asn) + '_' + str(i) + '.txt')
        if os.path.isfile(file_name):
            with open(file_name) as file_data:
                r = js.load(file_data)
            all_pieces.append(r)
            found_files = True
    if found_files is True:
        return all_pieces

    # Get AS routing history
    print('[+] Downloading AS history for AS' + str(asn))
    url = 'https://stat.ripe.net/data/announced-prefixes/data.json'
    payload = {'resource': 'AS' + str(asn), 'starttime': str(history_start),
               'endtime': history_end, 'soft_limit': 'ignore'}
    headers = {'User-Agent': 'Mozilla/5.0'}
    r = requests.get(url, params=payload, headers=headers)
    if r.status_code == requests.codes.ok:
        file_name = path.joinpath(str(asn) + '.txt')
        file = open(file_name, 'w')
        file.write(js.dumps(r.json()))
        file.close()
        return [r.json()]
    else:  # Could not download AS history, try in smaller pieces
        pieces = []
        start_epoch = gv.history_boundaries[1]
        end_epoch = gv.history_boundaries[3]
        interval = int((end_epoch - start_epoch)/15)
        new_start = start_epoch
        new_end = start_epoch + interval
        while(new_end <= end_epoch):
            history_start = time.strftime('%Y-%m-%dT%H:%M', time.gmtime(
                new_start))
            history_end = time.strftime('%Y-%m-%dT%H:%M', time.gmtime(
                new_end))
            payload = {'resource': 'AS' + str(asn),
                       'starttime': str(history_start), 'endtime': history_end,
                       'soft_limit': 'ignore'}
            r = requests.get(url, params=payload, headers=headers)
            pieces.append(r)
            new_start = new_end
            new_end = new_end + interval

        index = 1
        AS_history = []
        for i in pieces:
            if i.status_code == requests.codes.ok:
                filename = path.joinpath(str(asn) + '_' + str(index) + '.txt')
                file = open(filename, 'w')
                file.write(js.dumps(i.json()))
                file.close()
                AS_history.append(i.json())
            else:
                print('[-] Could not get AS history, status code ' +
                      str(i.status_code) + ' for AS' + str(asn) + ' piece ' +
                      str(index))
                return None
            index += 1
        return AS_history


# Get prefix history from RIPEstat
def get_prefix_routing_history(prefix):
    """Get prefix history for specified prefix from RIPEstat

    Get prefix history between history start and history end from
    RIPEstat in json format.

    Parameters:
        prefix (string): IPv4 or IPv6 prefix

    Returns:
        dataframe: prefix history as dataframe
    """

    history_start = gv.history_boundaries[0]
    history_end = gv.history_boundaries[2]

    # Check if file exists
    p = prefix.replace('/', '-')
    p = p.replace(':', '^%')
    path = config.data_directory.joinpath('prefix_history')
    file_name = path.joinpath(str(p) + '.txt')

    if os.path.isfile(file_name):
        with open(file_name) as file_data:
            r = js.load(file_data)
        return r

    # Otherwise get history
    print('[+] Downloading prefix history for prefix ' + prefix)
    url = 'https://stat.ripe.net/data/routing-history/data.json'
    payload = {'min_peers': 0, 'resource': prefix, 'starttime': history_start,
               'endtime': history_end, 'soft_limit': 'ignore'}
    r = requests.get(url, params=payload)  # Get info in JSON format
    if r.status_code == requests.codes.ok:
        file = open(file_name, 'w')
        file.write(js.dumps(r.json()))
        file.close()
        return r.json()
    else:
        print('[-] Could not get prefix history, status code ' +
              str(r.status_code) + ' for ' + prefix)
        return None


# Get prefix blacklist information from RIPEstat
def get_prefix_blacklist_data(prefix):
    """Get prefix blacklist data for specified prefix from RIPEstat

    Get blacklist data between history start and history end from
    RIPEstat in json format.

    Parameters:
        prefix (string): IPv4 or IPv6 prefix

    Returns:
        dataframe: prefix history as dataframe
    """

    history_start = gv.history_boundaries[0]
    history_end = gv.history_boundaries[2]

    # Check if file exists
    p = prefix.replace('/', '-')
    p = p.replace(':', '^%')
    path = config.data_directory.joinpath('blacklist_data')
    file_name = path.joinpath(str(p) + '.txt')

    if os.path.isfile(file_name):
        with open(file_name) as file_data:
            r = js.load(file_data)
        return r

    # Otherwise get history
    print('[+] Downloading blacklist data for prefix ' + prefix)
    url = 'https://stat.ripe.net/data/blacklist/data.json'
    payload = {'min_peers': 0, 'resource': prefix, 'starttime': history_start,
               'endtime': history_end, 'soft_limit': 'ignore'}
    r = requests.get(url, params=payload)  # Get info in JSON format
    if r.status_code == requests.codes.ok:
        file = open(file_name, 'w')
        file.write(js.dumps(r.json()))
        file.close()
        return r.json()
    else:
        print('[-] Could not get blacklist data, status code ' +
              str(r.status_code) + ' for ' + prefix)
        return None


# Load GeoLite2 files
def load_geolite_data():
    """Load GeoLite2 data and convert into useful dictionaries

    Returns:
        list: list containing geolite data in following order:
              ip_asn, ip_country
    """

    geolite = config.data_directory.joinpath('geolite')
    asn_path = geolite.joinpath('GeoLite2-ASN-CSV_20190528')
    country_path = geolite.joinpath('GeoLite2-Country-CSV_20190528')

    asn_ipv4 = asn_path.joinpath('GeoLite2-ASN-Blocks-IPv4.csv')
    asn_ipv6 = asn_path.joinpath('GeoLite2-ASN-Blocks-IPv6.csv')
    country_ipv4 = country_path.joinpath('GeoLite2-Country-Blocks-IPv4.csv')
    country_ipv6 = country_path.joinpath('GeoLite2-Country-Blocks-IPv6.csv')
    country_loc = country_path.joinpath('GeoLite2-Country-Locations-en.csv')

    filenames = [asn_ipv4, asn_ipv6, country_ipv4, country_ipv6, country_loc]
    data_files = []
    print('[+] Loading GeoLite2 data')
    for i in filenames:
        data_files.append(pd.read_csv(i, sep=',', ))

    # Convert data frames into dicionaries for faster look up
    print('[+] Converting GeoLite2 data to dictionaries')
    ip_asn = dict()
    d = pd.concat([data_files[0], data_files[1]])
    for i in range(0, len(d)):
        asn = d.iloc[i]['autonomous_system_number']
        if asn in ip_asn.keys():
            ip_asn[asn].append(d.iloc[i]['network'])
        else:
            ip_asn[asn] = [d.iloc[i]['network']]

    country_codes = dict()
    d = data_files[4]
    for i in range(0, len(d)):
        geoname = d.iloc[i]['geoname_id']
        country_codes[geoname] = d.iloc[i]['country_iso_code']

    ip_country = dict()
    d = pd.concat([data_files[2], data_files[3]])
    for i in range(0, len(d)):
        prefix = d.iloc[i]['network']
        if prefix in ip_country.keys():
            print('panic')
        else:
            ip_country[prefix] = dict()

            geo = d.iloc[i]['geoname_id']
            try:
                ip_country[prefix]['geoname_id'] = country_codes[geo]
            except KeyError:
                ip_country[prefix]['geoname_id'] = geo

            geo = d.iloc[i]['registered_country_geoname_id']
            try:
                ip_country[prefix]['registered_country_geoname_id'] = \
                    country_codes[geo]
            except KeyError:
                ip_country[prefix]['registered_country_geoname_id'] = geo

            geo = d.iloc[i]['represented_country_geoname_id']
            try:
                ip_country[prefix]['represented_country_geoname_id'] = \
                    country_codes[geo]
            except KeyError:
                ip_country[prefix]['represented_country_geoname_id'] = geo

    return [ip_asn, ip_country]


# Load IP2Location LITE data
def load_ip2lite_data():
    """Load IP2Location LITE data

    Returns:
        list: list containg IP2Location LITE data in following order: asn, db1
    """

    print('[+] Loading IP2Location LITE data')
    ip2lite = config.data_directory.joinpath('IP2Lite')
    asn_ipv4 = ip2lite.joinpath('IP2LOCATION-LITE-ASN.CSV')
    asn_ipv6 = ip2lite.joinpath('IP2LOCATION-LITE-ASN.IPV6.CSV')
    db1_ipv4 = ip2lite.joinpath('IP2LOCATION-LITE-DB1.CSV')
    db1_ipv6 = ip2lite.joinpath('IP2LOCATION-LITE-DB1.IPV6.CSV')
    data_files = []
    data_files.append(pd.read_csv(asn_ipv4, sep=',', header=None))
    data_files.append(pd.read_csv(asn_ipv6, sep=',', header=None))
    data_files.append(pd.read_csv(db1_ipv4, sep=',', header=None))
    data_files.append(pd.read_csv(db1_ipv6, sep=',', header=None))

    return data_files


# Get latest extended RSEF files for each irr
def get_RSEF_listings():
    """Get latest extended RSEF files for each irr

    Returns:
        list: list of dataframes in following order:
              'ripe', 'arin', 'lacnic', 'apnic', 'afrinic'
    """
    directory = config.data_directory.joinpath('rsef')
    print('[+] Downloading RSEF statistics')
    # Ripe
    url = ('https://ftp.ripe.net/pub/stats/ripencc/' +
           'delegated-ripencc-extended-latest')
    ripe = requests.get(url)
    file_name = directory.joinpath('ripe.txt')
    with open(file_name, 'w') as file:
        file.write(ripe.text)

    # Arin
    url = 'https://ftp.arin.net/pub/stats/arin/delegated-arin-extended-latest'
    arin = requests.get(url)
    file_name = directory.joinpath('arin.txt')
    with open(file_name, 'w') as file:
        file.write(arin.text)

    # Lacnic
    url = ('http://ftp.lacnic.net/pub/stats/lacnic/' +
           'delegated-lacnic-extended-latest')
    lacnic = requests.get(url)
    file_name = directory.joinpath('lacnic.txt')
    with open(file_name, 'w') as file:
        file.write(lacnic.text)

    # Apnic
    url = 'https://ftp.apnic.net/stats/apnic/delegated-apnic-extended-latest'
    apnic = requests.get(url)
    file_name = directory.joinpath('apnic.txt')
    with open(file_name, 'w') as file:
        file.write(apnic.text)

    # Afrinic
    url = ('http://ftp.afrinic.net/pub/stats/afrinic/' +
           'delegated-afrinic-extended-latest')
    afrinic = requests.get(url)
    file_name = directory.joinpath('afrinic.txt')
    with open(file_name, 'w') as file:
        file.write(afrinic.text)

    # Load all files into list
    irr_names = ['ripe', 'arin', 'lacnic', 'apnic', 'afrinic']
    data_files = []
    for i in range(0, len(irr_names)):
        file_name = directory.joinpath(irr_names[i] + '.txt')
        file_data = pd.read_csv(file_name, sep='|',
                                header=None, names=['registry', 'cc', 'type',
                                                    'start', 'value', 'date',
                                                    'status', 'extension'],
                                comment='#')

        # Remove first four lines as they do not contain data but summaries
        data_files.append(file_data.iloc[4:, ])

    return data_files


# Get RPKI data
def get_rpki_info():
    """Get RPKI data from RPKI validator

    Returns:
        dataframe: RPKI data in json format
    """

    print('[+] Downloading RPKI validator data')
    url = 'https://rpki-validator.ripe.net/api/export.json'
    headers = {'Accept': 'application/json'}
    # text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
    roas = requests.get(url, headers=headers).json()['roas']
    rpki = dict()
    for i in roas:
        prefix = i['prefix']
        if prefix in rpki.keys():
            rpki[prefix].append(i)
        else:
            rpki[prefix] = [i]
    return rpki
