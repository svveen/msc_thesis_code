from pathlib import Path

# Global variables
data_directory = Path(__file__).parent.joinpath('Data')
prefix_history = dict()  # Keep prefix histories
AS_history = dict()  # Keep AS histories
blacklist_data = dict()  # Keep blacklist data
