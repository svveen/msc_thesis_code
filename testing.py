import stringdist as sd
import calendar
import time
import statistics

import event_attributes as ea
import collect_data as cd


# Check if AS countries in RSEF listing, geolite data, and ip2location
# is the same
def check_as_country_to_rsef(hijack_attributes):
    """Compare AS country data from each data source

    Parameters:
        hijack_attributes (dict) = hijack attributes created in main
    """

    AS_country = dict()
    for i in hijack_attributes:
        det = hijack_attributes[i]['country detected ASN']
        exp = hijack_attributes[i]['country expected ASN']
        if hijack_attributes[i]['detected ASN'] not in AS_country.keys():
            AS_country[hijack_attributes[i]['detected ASN']] = det
        if hijack_attributes[i]['expected ASN'] not in AS_country.keys():
            AS_country[hijack_attributes[i]['expected ASN']] = exp

    ip2lite_geolite_match = 0
    ip2lite_rsef_match = 0
    geolite_rsef_match = 0
    geolite_rsef_none = 0
    ip2lite_rsef_none = 0
    total = 0
    for i in AS_country:
        total += 1
        country = AS_country[i]
        if ea.check_same_country(country[0], country[2]):
            ip2lite_geolite_match += 1
        if ea.check_same_country(country[0], [country[1]]):
            geolite_rsef_match += 1
        else:
            if country[0] is None:
                if type(country[1]) != str or country[1] == '':
                    geolite_rsef_match += 1
                else:
                    geolite_rsef_none += 1
        if ea.check_same_country(country[2], [country[1]]):
            ip2lite_rsef_match += 1
        else:
            if country[2] is None:
                if type(country[1]) != str or country[1] == '':
                    ip2lite_rsef_match += 1
                else:
                    ip2lite_rsef_none += 1

    grp = round(geolite_rsef_match/total*100, 2)
    grn = round(geolite_rsef_none/total*100, 2)
    gip = round(ip2lite_geolite_match/total*100, 2)
    irp = round(ip2lite_rsef_match/total*100, 2)
    irn = round(ip2lite_rsef_none/total*100, 2)
    print('GeoLite2 data matches with RIR statistics for ' + str(grp)
          + '% of ASes and has missing GeoLite2 data for ' + str(grn)
          + '% of ASes')
    print('IP2Location LITE data matches with RIR statistics for ' + str(irp)
          + '% of ASes and has missing IP2Location LITE data for ' + str(irn)
          + '% of ASes')
    print('IP2Location LITE and GeoLite2 match for ' + str(gip)
          + '% of ASes')


# Check if prefix countries in geolite data and ip2location is the same
def check_prefix_country_statistics(hijack_attributes):
    """Compare prefix country data from each data source

    Parameters:
        hijack_attributes (dict) = hijack attributes created in main
    """

    prefix_country = dict()
    for i in hijack_attributes:
        country = hijack_attributes[i]['country of hijacked prefix']
        if (hijack_attributes[i]['detected advertisement']
                not in prefix_country.keys()):
            prefix_country[hijack_attributes[i]['detected advertisement']] = \
                country

    ip2lite_geolite_match = 0
    ip2lite_rsef_match = 0
    geolite_rsef_match = 0
    geolite_rsef_none = 0
    ip2lite_rsef_none = 0
    total = 0

    for i in prefix_country:
        total += 1
        country = prefix_country[i]
        if ea.check_same_country(country[0], [country[2]]):
            ip2lite_geolite_match += 1
        if ea.check_same_country(country[0], [country[1]]):
            geolite_rsef_match += 1
        else:
            if country[0] == '':
                if type(country[1]) != str or country[1] == '':
                    geolite_rsef_match += 1
                else:
                    geolite_rsef_none += 1
        if ea.check_same_country([country[2]], [country[1]]):
            ip2lite_rsef_match += 1
        else:
            if country[2] == '-':
                if type(country[1]) != str or country[1] == '':
                    ip2lite_rsef_match += 1
                else:
                    ip2lite_rsef_none += 1

    grp = round(geolite_rsef_match/total*100, 2)
    grn = round(geolite_rsef_none/total*100, 2)
    gip = round(ip2lite_geolite_match/total*100, 2)
    irp = round(ip2lite_rsef_match/total*100, 2)
    irn = round(ip2lite_rsef_none/total*100, 2)
    print('GeoLite2 data matches with RIR statistics for ' + str(grp)
          + '% of prefixes and has missing GeoLite2 data for ' + str(grn)
          + '% of prefixes')
    print('IP2Location LITE data matches with RIR statistics for ' + str(irp)
          + '% of prefixes and has missing IP2Location LITE data for ' +
          str(irn) + '% of prefixes')
    print('IP2Location LITE and GeoLite2 match for ' + str(gip)
          + '% of prefixes')


# Find the average time for which a prefix is announced to get an indication
# for a normally announced prefix
def get_average_time_prefix_announcement(hijack_attributes):
    """Find the average time for which a prefix is announced to get an
    indication for a normally announced prefix

    Parameters:
        hijack_attributes (dict): hijack attributes created in main
    """

    AS_history = dict()
    for i in hijack_attributes:
        hijack = hijack_attributes[i]
        det = hijack['detected ASN']
        exp = hijack['expected ASN']
        if det not in AS_history.keys():
            AS_history[det] = cd.get_AS_routing_history(det)
        if exp not in AS_history.keys():
            AS_history[exp] = cd.get_AS_routing_history(exp)

    all_period_lengths = []  # lengths of each period
    all_total_times = []  # for each prefix total announcement time
    total_prefixes = 0  # total number of announced prefixes
    for i in AS_history:
        pieces = AS_history[i]
        if pieces is None or pieces == []:
            continue
        else:
            for piece in pieces:
                timelines = piece['data']['prefixes']
                for j in timelines:
                    add_prefix = False
                    prefix_total = 0
                    for k in j['timelines']:
                        e = calendar.timegm(time.strptime(
                                k['endtime'], '%Y-%m-%dT%H:%M:%S'))
                        s = calendar.timegm(time.strptime(
                                k['starttime'], '%Y-%m-%dT%H:%M:%S'))
                        if (e-s) > 0:
                            prefix_total += (e-s)
                            all_period_lengths.append(e-s)
                            add_prefix = True
                    if add_prefix is True:
                        all_total_times.append(prefix_total)
                        total_prefixes += 1

    average_periods_per_prefix = len(all_period_lengths)/total_prefixes
    average_total_per_prefix = sum(all_total_times)/total_prefixes
    average_total_weeks = average_total_per_prefix/604800
    AS_median_periods = statistics.median(all_period_lengths)
    AS_median_weeks_periods = AS_median_periods/604800
    AS_median_total = statistics.median(all_total_times)
    AS_median_weeks_total = AS_median_total/604800

    print('The average total time a prefix is announced is ' +
          str(average_total_per_prefix) + ' seconds. This amounts to ' +
          str(average_total_weeks) + ' weeks')
    print('The median total time a prefix is announced is ' +
          str(AS_median_total) + ' seconds')
    print('This amounts to ' + str(AS_median_weeks_total) + ' weeks')
    print('On average there are ' + str(average_periods_per_prefix) +
          ' periods in which a prefix is announced. The median length of ' +
          'these periods is ' + str(AS_median_weeks_periods) + ' weeks')


# Tests string distance function on prefix examples
def test_string_distance_functions_on_prefixes():
    """Tests string distance function on prefix examples
    """

    p1 = '12.0.2.0'
    p2 = '192.0.2.0'
    print("Damerau-Levenshtein: " + p1 + ' - ' + p2 + ' = ' +
          str(sd.rdlevenshtein(p1, p2)))

    p1 = '192.0.2.0'
    p2 = '192.0.3.0'
    print("Damerau-Levenshtein: " + p1 + ' - ' + p2 + ' = ' +
          str(sd.rdlevenshtein(p1, p2)))

    p1 = '192.0.2.0'
    p2 = '19.20.2.0'
    print("Damerau-Levenshtein: " + p1 + ' - ' + p2 + ' = ' +
          str(sd.rdlevenshtein(p1, p2)))

    p1 = '192.0.2.0'
    p2 = '192.0.23.0'
    print("Damerau-Levenshtein: " + p1 + ' - ' + p2 + ' = ' +
          str(sd.rdlevenshtein(p1, p2)))

    p1 = '192.0.2.0'
    p2 = '19.20.23.0'
    print("Damerau-Levenshtein: " + p1 + ' - ' + p2 + ' = ' +
          str(sd.rdlevenshtein(p1, p2)))

    p1 = '192.0.2.0'
    p2 = '198.51.100.0'
    print("Damerau-Levenshtein: " + p1 + ' - ' + p2 + ' = ' +
          str(sd.rdlevenshtein(p1, p2)))


# Find difference in AS relationships between two months
def find_changed_relations(labeled_dataset):
    """ Find difference in AS relationships between two months

    Parameters:
        labeled_dataset (DataFrame)
    """

    changed_relations_det_exp = dict()
    changed_cc_det_exp = dict()
    changed_cc_exp_det = dict()
    changed_path_relations = dict()

    for i in range(0, len(labeled_dataset)):
        hijack = labeled_dataset.iloc[i]
        if (hijack['relation detected-expected'] !=
                hijack['relation detected-expected nm']):
            changed_relations_det_exp[i] = \
                (hijack['relation detected-expected'],
                 hijack['relation detected-expected nm'])
        if (hijack['exp in customer cone of det'] !=
                hijack['exp in customer cone of det nm']):
            changed_cc_exp_det[i] = (hijack['exp in customer cone of det'],
                                     hijack['exp in customer cone of det nm'])
        if (hijack['det in customer cone of exp'] !=
                hijack['det in customer cone of exp nm']):
            changed_cc_det_exp[i] = (hijack['det in customer cone of exp'],
                                     hijack['det in customer cone of exp nm'])
        if hijack['path relations'] != hijack['path relations nm']:
            changed_path_relations[i] = (hijack['path relations'],
                                         hijack['path relations nm'])

    print('Changed relations between detected and expected AS: ' +
          str(len(changed_relations_det_exp)))
    print('Change expected in customer cone of detected: ' +
          str(len(changed_cc_exp_det)))
    print('Change detected in customer cone of expected: ' +
          str(len(changed_cc_det_exp)))
    print('Changed path relations: ' + str(len(changed_path_relations)))
