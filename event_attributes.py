import calendar
import time

import mpu

import asn_attributes as aa
import collect_data as cd
import config


# Get the time of a month later than given time
def get_next_month(starttime):
    """Get UTC format data and time of month later than given time.
    Used to get relation data from next month in case it changed during
    the month in which the event occurs.

    Parameters:
        starttime (string): hijack start time

    Returns:
        string: string giving data and time month after start time

    """
    next_month = starttime.split(' ')[0]
    t = starttime.split(' ')[1]
    if next_month[5:7] == '12':
        next_month = next_month.replace('-', '')
        next_month = str(int(next_month[0:4]) + 1) + '01' + next_month[6:8]
    else:
        next_month = str(int(next_month.replace('-', '')) + 100)
    next_month = (next_month[0:4] + '-' + next_month[4:6] + '-'
                  + next_month[6:8] + ' ' + t + ' UTC')
    return next_month


# Check for MOAS conflict for a prefix at a given time
def check_MOAS_conflict(prefix, timestamp):
    """Check if there is a MOAS conflict at the time of the hijack

    Parameters:
        prefix (string): IP prefix
        timestamp (int): event start time converted to epoch

    Returns:
        list: list of ASes announcing prefix at given time and start and
              end time for each announcement
    """

    if prefix in config.prefix_history.keys():
        timelines = config.prefix_history[prefix]
    else:
        hist = cd.get_prefix_routing_history(prefix)
        if hist is not None:
            config.prefix_history[prefix] = hist['data']['by_origin']
            timelines = hist['data']['by_origin']
        if hist is None:
            return None
    moas = []
    for i in timelines:
        for j in i['prefixes']:
            if j['prefix'] == prefix:
                # Check if this is announced during event
                for k in j['timelines']:
                    e = calendar.timegm(time.strptime(
                            k['endtime'], '%Y-%m-%dT%H:%M:%S'))
                    s = calendar.timegm(time.strptime(
                            k['starttime'], '%Y-%m-%dT%H:%M:%S'))
                    if timestamp >= s and timestamp <= e:
                        moas.append((i['origin'], s, e))
    return moas


# Get relations between ASes in AS path
def get_as_path_relations(starttime, as_path):
    """Get relations between ASes in AS path

    Parameters:
        starttime (string): YYYY-mm-dd HH:MM:SS UTC
        as_path (string): AS path

    Returns:
        string: AS path relations
    """

    as_list = as_path.split(" ")
    path_relations = ''
    for i in range(0, len(as_list)-1):
        as1 = as_list[i]
        as2 = as_list[i+1]
        if as1 != as2:
            r = aa.get_relation_ASes(starttime, as1, as2)
            path_relations += r + ' '
        else:
            path_relations += 'self '
    return path_relations


# Checks if AS path is valley free
def check_valley_free_path(path_relations):
    """Check if AS path is valley free

    Parameters:
        path_relations (string): result from get_as_path_relations

    Returns:
        bool: True if path is valley free, False otherwise
    """

    # Valley free patterns:
    # n*c2p + m*p2c
    # n*c2p + p2p + m*p2c
    # where n and m ≥ 0

    if 'no relation' in path_relations:
        return False

    p2p = False  # seen p2p relation
    p2c = False  # seen p2c relation
    relations = path_relations.strip().split(" ")
    for i in relations:
        if i == 'p2p':
            if p2c is True:
                return False
            if p2p is False:
                p2p = True
            else:  # Found two times p2p in one path
                return False
        if i == 'p2c':
            if p2c is False:
                p2c = True
        if i == 'c2p':
            if p2p is True or p2c is True:  # Not valley free
                return False
    return True


# Check if the detected AS prepended the path with his asn
def check_path_prepending(as_path, asn):
    """Check how many times asn appears at the start of AS path

    Parameters:
        as_path (string): AS path
        asn (string/int): AS number

    Returns:
        int: number of times asn appears at the start of the path
    """

    asn = str(asn)
    path = as_path.split(" ")
    path.reverse()
    count = 0
    for i in path:
        if i == asn:
            count += 1
        else:
            break
    return count


# Check if the AS path is continuous
def check_continuous_path(path_relations, path_relations_nm):
    """Check if the AS path is continuous

    Parameters:
        path_relations (string): Path relations in month of hijack
        path_relations (string): Path relations month after hijacks

    Returns:
        (bool/string): False if not continuous, True if continuous,
                       otherwise `-'
    """
    if 'no relation' in path_relations and 'no relation' in path_relations_nm:
        if path_relations == path_relations_nm:
            return False
        else:
            path = path_relations.replace('no relation', 'no_relation').split()
            path_nm = path_relations_nm.replace(
                    'no relation', 'no_relation').split()
            for i in range(0, len(path)):
                if path[i] == 'no_relation' and path_nm[i] == 'no_relation':
                    return False
            return '-'
    else:
        return True


# Check if two country lists have a common country
def check_same_country(country_list1, country_list2):
    """Check if two country lists have a country in common

    Parameters:
        country_list1 (list): list of strings representing countries
        country_list2 (list): list of strings representing countries

    Returns:
        bool: True if lists share a country, False otherwise
    """

    if country_list1 == country_list2:
        return True
    if country_list1 is None or country_list2 is None:
        return False

    country1 = mpu.datastructures.flatten(country_list1)
    country2 = mpu.datastructures.flatten(country_list2)

    for i in country1:
        if i in country2:
            return True
    return False


# Get the duration of the hijack using AS and prefix history
def get_hijack_duration(timestamp, asn, prefix):
    """Get the duration of the hijack using AS and prefix history

    Paramters:
        timestamp (int): hijack start time in epoch format
        asn (int/string): AS number
        prefix (string): detected advertisement

    Returns:
        int: duration of hijack in seconds
    """

    if prefix in config.prefix_history.keys():
        prefix_timelines = config.prefix_history[prefix]
    else:
        hist = cd.get_prefix_routing_history(prefix)
        if hist is not None:
            config.prefix_history[prefix] = hist['data']['by_origin']
            prefix_timelines = hist['data']['by_origin']
        if hist is None:
            prefix_timelines = None

    prefix_duration = 0
    for i in prefix_timelines:
        if i['origin'] == str(asn):
            for j in i['prefixes']:
                if j['prefix'] == prefix:
                    timelines = j['timelines']
                    for k in timelines:
                        # Turn timestamp into required format
                        s = calendar.timegm(time.strptime(
                            k['starttime'], '%Y-%m-%dT%H:%M:%S'))
                        e = calendar.timegm(time.strptime(
                            k['endtime'], '%Y-%m-%dT%H:%M:%S'))
                        # If hijack happens during this period, add duration
                        timedif = abs(timestamp - s)
                        if timestamp <= e and timedif <= 86400*2:
                            prefix_duration += e-s

    if asn in config.AS_history.keys():
        asn_timelines = config.AS_history[asn]
    else:
        hist = cd.get_AS_routing_history(asn)
        if hist is not None:
            asn_timelines = []
            for i in hist:
                for j in i['data']['prefixes']:
                    asn_timelines.append(j)
            config.AS_history[asn] = asn_timelines
        if hist is None:
            config.AS_history[asn] = None
            asn_timelines = None

    asn_duration = 0
    for i in asn_timelines:
        if i['prefix'] == prefix:
            timelines = i['timelines']
            for j in timelines:
                # Turn timestamp into required format
                s = calendar.timegm(time.strptime(
                    j['starttime'], '%Y-%m-%dT%H:%M:%S'))
                e = calendar.timegm(time.strptime(
                    j['endtime'], '%Y-%m-%dT%H:%M:%S'))
                # If hijack happens during this period, add duration
                timedif = abs(timestamp - s)
                if timestamp <= e and timedif <= 86400*2:
                    asn_duration += e-s
    return(prefix_duration, asn_duration)


# Checks if AS23456 is part of the AS path
def check_as23456(as_path):
    """Check if AS23456 is part of AS path

    Parameters:
        as_path (string): AS path

    Returns:
        bool: True if AS23456 in path, False otherwise
    """

    if (as_path.startswith('23456 ') or as_path.endswith(' 23456') or
            ' 23456 ' in as_path):
        return True
    else:
        return False
