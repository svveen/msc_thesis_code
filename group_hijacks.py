from collections import Counter


# Group hijacks by given feature
def group_by_feature(hijack_dataset, feature):
    """Group hijacks by given feature and create new column with group IDs

    Parameters:
        hijack_dataset (DataFrame): hijack dataset
        feature (string): string with column name

    Returns:
        DataFrame: hijacks dataset with extra column for feature

    """
    feature_list = list(hijack_dataset[feature].unique())  # unique values
    hijack_dataset['group ID: ' + feature] = '-'  # new column
    current_ID = 0

    for i in feature_list:
        # get all hijacks per feature value and give them same ID
        group = hijack_dataset[hijack_dataset[feature] == i]
        indices = list(group.index)
        for j in indices:
            hijack_dataset.at[j, 'group ID: ' + feature] = current_ID
        current_ID += 1
    return hijack_dataset


# Group related events by giving them a group ID
def group_by_detected_AS_and_time(hijack_dataset):
    """Group hijacks that have the same detected ASN and happen within
    two hours of eachother.

    Parameters:
        hijack_dataset (DataFrame): hijack dataset

    Returns:
        DataFrame: hijack_dataset with extra column for group ID

    """
    ASN_list = list(hijack_dataset['detected ASN'].unique())
    hijack_dataset['group ID: AS + time'] = '-'  # New column for group ID
    current_ID = 0
    for i in ASN_list:
        # Get all events per detected ASN
        group = hijack_dataset[hijack_dataset['detected ASN'] == i]
        indices = list(group.index)
        if len(group) > 1:  # More than one event, check if related
            group = group.sort_values(by=['timestamp'])  # Sort by time
            # Set group ID for each row
            hijack_dataset.at[indices[0], 'group ID: AS + time'] = current_ID
            for j in range(1, len(indices)):  # Compare timestamps
                # Related events happen within 2 hours of each other
                t1 = hijack_dataset.iloc[indices[j]]['timestamp']
                t2 = hijack_dataset.iloc[indices[j-1]]['timestamp']
                if abs(t1 - t2) > 2*3600:
                    current_ID += 1
                hijack_dataset.at[indices[j], 'group ID: AS + time'] = \
                    current_ID
            current_ID += 1
        else:  # Only one event, assign group ID
            for j in indices:
                hijack_dataset.at[j, 'group ID: AS + time'] = current_ID
                current_ID += 1
    return hijack_dataset


# Group hijacks that are related because ASes attack the same prefixes
def group_by_detected_advertisement_set(hijack_dataset):
    """Group hijacks that are related because the detected ASes attack the
    same prefixes

    Parameters:
        hijack_dataset (DataFrame): hijack_dataset with group IDs for
                                    detected advertisement and detected ASes

    Returns:
        DataFrame: hijack_dataset with extra group ID for this category
    """
    id_name = 'group ID: detected advertisement set'
    hijack_dataset[id_name] = '-'
    prefix_counts = dict(Counter(
            hijack_dataset['group ID: detected advertisement']))
    current_ID = 0

    matches = dict()
    for i in prefix_counts:
        group = hijack_dataset[
                hijack_dataset['group ID: detected advertisement'] == i]
        index = group.index
        if prefix_counts[i] == 1:  # prefix is hijacked one time
            hijack_dataset.at[index, id_name] = current_ID
            current_ID += 1
        else:  # prefix is hijacked multiple times
            # if prefix is hijacked multiple times by the same AS
            if len(list(group['detected ASN'].unique())) == 1:
                for j in index:
                    hijack_dataset.at[j, id_name] = current_ID
                    current_ID += 1
            # else prefix is hijacked by multiple ASes
            else:
                matches[i] = dict()
                matches[i]['indices'] = list(index)
                matches[i]['ASes'] = list(group['detected ASN'])
                matches[i]['unique ASes'] = list(
                        group['detected ASN'].unique())

    unique_ases = dict()
    # hijacks with the same group of unique ASes get the same ID
    for i in matches:
        match = matches[i]
        unique = str(match['unique ASes'])
        if unique in unique_ases.keys():
            group_id = unique_ases[unique]
            for j in match['indices']:
                hijack_dataset.at[j, id_name] = group_id
        else:
            unique_ases[unique] = current_ID
            for j in match['indices']:
                hijack_dataset.at[j, id_name] = current_ID
            current_ID += 1

    # Remove cases where the group of detected ASes have only unique ASes
    # or where the number of detected ASes are not a divisor of the number of
    # hijacks
    group_count = Counter(hijack_dataset[id_name])
    for i in group_count.keys():
        if group_count[i] > 1:
            group = hijack_dataset[hijack_dataset[id_name] == i]
            if ((len(group['detected ASN'].unique()) == len(group)) or
                    (len(group) % len(group['detected ASN'].unique()) > 0)):
                indices = list(group.index)
                for j in range(1, len(indices)):
                    hijack_dataset.at[indices[j], id_name] = current_ID
                    current_ID += 1

    return hijack_dataset


# Find hijacks of prefixes where the detected AS is later the expected AS
def group_by_detected_expected_switch(hijack_dataset):
    """Find hijacks of prefixes where the detected AS becomes the expected AS
    or the expected AS becomes the detected AS

    Parameters:
        hijack_dataset (DataFrame): hijack data set

    Returns:
        DataFrame: hijack data set with group IDs for this category
    """
    id_name = 'group ID: detected expected switch'
    hijack_dataset[id_name] = '-'
    prefix_counts = dict(Counter(
            hijack_dataset['group ID: detected advertisement']))
    current_ID = 0

    for i in prefix_counts:
        group = hijack_dataset[
                hijack_dataset['group ID: detected advertisement'] == i]
        index = group.index
        if prefix_counts[i] == 1:  # prefix is hijacked one time
            hijack_dataset.at[index, id_name] = current_ID
            current_ID += 1
        else:  # prefix is hijacked multiple times
            detected = set(group['detected ASN'])
            expected = set(group['expected ASN'])
            # No switch
            if detected.intersection(expected) == set():
                for j in index:
                    hijack_dataset.at[j, id_name] = current_ID
                    current_ID += 1
            # else detected AS and expected AS switch
            else:
                for j in index:
                    hijack_dataset.at[j, id_name] = current_ID
                current_ID += 1

    return hijack_dataset


# Call all functions to group hijacks
def group_hijacks(hijack_dataset):
    """Calls all functions to  group hijacks

    Parameters:
        hijack_dataset (DataFrame): hijack dataset from main

    Returns:
        DataFrame: hijack_dataset with group IDs
    """
    hijack_dataset = group_by_detected_AS_and_time(hijack_dataset)
    hijack_dataset = group_by_feature(hijack_dataset, 'detected ASN')
    hijack_dataset = group_by_feature(hijack_dataset, 'expected ASN')
    hijack_dataset = group_by_feature(hijack_dataset, 'detected advertisement')
    hijack_dataset = group_by_feature(hijack_dataset, 'expected prefix')
    hijack_dataset = group_by_feature(hijack_dataset,
                                      'detected AS organization ID')
    hijack_dataset = group_by_feature(hijack_dataset,
                                      'expected AS organization ID')
    hijack_dataset = group_by_detected_advertisement_set(hijack_dataset)
    hijack_dataset = group_by_detected_expected_switch(hijack_dataset)

    return hijack_dataset
