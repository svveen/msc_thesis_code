import pandas as pd

import data
import label_hijacks as lh
import group_hijacks as gh
import generate_feature_results as gfr
import generate_relation_results as grr


# Get hijack attributes
hijack_attributes = dict()
bgpstream_hijacks = data.events[0]
for i in bgpstream_hijacks:
    print(i)
    hijack = bgpstream_hijacks[i]
    hijack_attributes[i] = lh.get_hijack_attributes(hijack)

# Make dataframe
hijack_dataset = pd.DataFrame()
for i in hijack_attributes:
    hijack = hijack_attributes[i]
    # Fix mistake from getting events
    org = hijack['expected organization']
    if org.count('(') != org.count(')'):
        hijack['expected organization'] = org[0:(len(org)-1)]
        org = hijack['detected organization']
        hijack['detected organization'] = org[0:(len(org)-1)]
    hijack_attributes[i] = hijack
    hijack_dataset = hijack_dataset.append(pd.Series(hijack),
                                           ignore_index=True)

# Group hijacks
hijack_dataset = gh.group_hijacks(hijack_dataset)

# Label hijacks
hijack_labels = lh.get_hijack_labels(hijack_dataset)
hijack_labels = grr.relation_labels(hijack_dataset, hijack_labels)


# Analyse hijacks - features
gfr.get_continuous_valley_paths(hijack_dataset)
gfr.get_hijack_duration_stats(hijack_dataset)
gfr.get_ip_version_distribution(hijack_dataset)
gfr.get_ratio_global_local(hijack_dataset)
gfr.get_relation_hijack_ASes(hijack_dataset)
gfr.graph_AS_global_picture(hijack_dataset)
gfr.graph_AS_per_RIR(hijack_dataset)
gfr.graph_cc_size_and_bgpmon_peers(hijack_dataset)
gfr.graph_path_length_and_prepending(hijack_dataset)
gfr.graph_path_length_bgpmon(hijack_dataset)
gfr.graph_prefix_length(hijack_dataset)
