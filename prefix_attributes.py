import calendar
import time

from netaddr import IPNetwork

import collect_data as cd
import data
import config


# Check if prefix is a special-use address prefix
def check_special_prefixes(prefix):
    """Check if prefix is special-use address

    Parameters:
        prefix (string): IP prefix

    Returns:
        bool: True if prefix is special-use address
    """

    special_prefix_ranges = ['0.0.0.0/8', '10.0.0.0/8', '100.64.0.0/10',
                             '127.0.0.0/8', '169.254.0.0/16', '172.16.0.0/12',
                             '192.0.0.0/24', '192.0.2.0/24', '192.31.196.0/24',
                             '192.52.193.0/24', '192.88.99.0/24',
                             '192.168.0.0/16', '192.175.48.0/24',
                             '198.18.0.0/15', '198.51.100.0/24',
                             '203.0.113.0/24', '224.0.0.0/4', '240.0.0.0/4',
                             '::ffff:0:0/96', '64:ff9b:1::/48', '2001::/23'
                             '::ffff:0:0:0/96', '64:ff9b::/96', '100::/64',
                             '2001::/32', '2001:20::/28', '2001:db8::/32',
                             '2002::/16', 'fc00::/7', 'fe80::/10', 'ff00::/8',
                             '2001:2::/48', '2001:3::/32', '2001:4:112::/48',
                             '2001:5::/32', '2001:10::/28',
                             '2620:4f:8000::/48']
    special_prefixes = ['255.255.255.255/32', '::/0', '::/128', '::1/128',
                        '2001:1::1/128', '2001:1::2/128']

    if prefix in special_prefixes:
        return True

    ip = IPNetwork(prefix)
    supernets = ip.supernet(4)
    for i in supernets:
        if str(i) in special_prefix_ranges:
            return True
    return False


# Get country of prefix from GeoLite2, RSEF and IP2Location LITE
def get_prefix_country(prefix):
    """Get country of prefix from GeoLite2, RSEF and IP2Location LITE

    Parameters:
        prefix (string): IP prefix

    Returns:
        list: list of country found for prefix in GeoLite2, RSEF and
              IP2Location LITE
    """

    ip = IPNetwork(prefix)
    supernets = ip.supernet(4)

    # Get country from geolite data
    ip_data = data.geolite[1]
    country_geolite = ''
    if prefix in ip_data.keys():
        country_geolite = list(ip_data[prefix].values())
    else:
        for i in supernets:
            n = str(i)
            if n in ip_data.keys():
                country_geolite = list(ip_data[n].values())
                break

    # Get country from rsef
    country_rsef = ''

    supernets.append(ip)  # Add prefix to list of supernets
    for i in supernets:
        network = str(i)
        pr = network.split('/')[0]  # Prefix only
        if ip.version == 4:  # IPv4
            length = 2**(32-int(network.split('/')[1]))  # Size of block
        else:  # IPv6
            length = int(network.split('/')[1])  # CIDR prefix length
        for listing in data.rsef:
            entries = listing[listing['start'] == pr]
            entries = entries[entries['value'] == length]
            if len(entries) > 0:
                country_rsef = entries['cc'].iloc[0]
                break
        if country_rsef != '':
            break

    if type(country_rsef) is float:
        country_rsef = ''

    # Get country from ip2lite
    if ip.broadcast is not None:
        ip_number_range = [int(ip.network), int(ip.broadcast)]
    else:
        if ip.size == 1:
            ip_number_range = [int(ip.network), int(ip.network)]
        if ip.size == 2:
            ip_number_range = [int(ip.network), int(ip.network)+1]

    if ip.version == 4:
        country_data = data.ip2lite[2]
    else:
        country_data = data.ip2lite[3]
    entries = country_data.loc[country_data[0].map(int) <= ip_number_range[1]]
    entries = entries.loc[entries[1].map(int) >= ip_number_range[0]]

    country_ip2lite = []
    for j in range(0, len(entries)):
        country_ip2lite.append(entries.iloc[j][2])
    if len(country_ip2lite) == 1:
        country_ip2lite = country_ip2lite[0]

    return [country_geolite, country_rsef, country_ip2lite]


# Check if prefix 1 is a subnet of prefix 2
def check_if_subnet(p1, p2):
    """Check if prefix 1 is subnet a of prefix 2

    Parameters:
        p1 (string): prefix 1
        p2 (string): prefix 2

    Returns:
        bool: True is p1 is subnet of p2, False otherwise
    """

    if p1 == p2:
        return False
    # Compare prefix lengths
    if int(p1.split('/')[1]) > int(p2.split('/')[1]):
        # Check if detected is subnet of expected
        supernets = IPNetwork(p1).supernet(int(p2.split('/')[1]))
        if IPNetwork(p2) in supernets:
            return True
    return False


# Check if a prefix is a subnet of a prefix that is normally announced by an AS
def check_announcing_subnet(asn, prefix, timestamp):
    """Check if prefix is a subnet of a prefix that is normally announced
    by the specified asn.

    Parameters:
        asn (string/int): AS number
        prefix (string): IP prefix
        timestamp (int): event start time converted to epoch

    Returns:
        list: list of prefixes that are announced at least six weeks and of
              which specified prefix is a subnet
    """

    if asn in config.AS_history.keys():
        timelines = config.AS_history[asn]
        if timelines is None:
            return None
    else:
        hist = cd.get_AS_routing_history(asn)
        if hist is not None:
            timelines = []
            for i in hist:
                for j in i['data']['prefixes']:
                    timelines.append(j)
            config.AS_history[asn] = timelines
        elif hist is None:
            config.AS_history[asn] = None
            return None

    # Get supernets for prefix
    ip = IPNetwork(prefix)
    supernets = ip.supernet(1)
    matches = []
    year_before = timestamp - 31556926
    week_after = timestamp + 604800
    # Check if any announcement in timelines is of prefix in supernets
    for i in timelines:
        if IPNetwork(i['prefix']) in supernets:
            # Found range, check if it is announced for more than six weeks
            total = 0
            for j in i['timelines']:
                # Turn timestamp into required format
                s = calendar.timegm(time.strptime(
                        j['starttime'], '%Y-%m-%dT%H:%M:%S'))
                e = calendar.timegm(time.strptime(
                        j['endtime'], '%Y-%m-%dT%H:%M:%S'))
                # if match in year before to week after event, append
                if year_before <= s and week_after >= s:
                    total += e - s
                    if total >= 6*604800 and i not in matches:
                        matches.append(i)
    return matches


# Check if a prefix is a supernet of a normally announced prefix.
def check_announcing_supernet(asn, prefix, timestamp):
    """Check if prefix is a supernet of a prefix that is normally announced
    by the specified asn.

    Parameters:
        asn (string/int): AS number
        prefix (string): IP prefix
        timestamp (int): event start time converted to epoch

    Returns:
        list: list of prefixes that are announced at least six weeks and of
              which specified prefix is a supernet
    """

    if asn in config.AS_history.keys():
        timelines = config.AS_history[asn]
        if timelines is None:
            return None
    else:
        hist = cd.get_AS_routing_history(asn)
        if hist is not None:
            timelines = []
            for i in hist:
                for j in i['data']['prefixes']:
                    timelines.append(j)
            config.AS_history[asn] = timelines
        if hist is None:
            config.AS_history[asn] = None
            return None

    ip = IPNetwork(prefix)
    matches = []
    year_before = timestamp - 31556926
    week_after = timestamp + 604800
    # Check if any announcement in timelines is a subnet of given prefix
    for i in timelines:
        timeline_prefix = IPNetwork(i['prefix'])
        if ip.prefixlen < timeline_prefix.prefixlen:
            supernets = timeline_prefix.supernet(ip.prefixlen)
        else:
            continue
        if ip in supernets:
            # Found more specific prefix, check if it is
            # announced for more than six weeks
            total = 0
            for j in i['timelines']:
                # Turn timestamp into required format
                s = calendar.timegm(time.strptime(
                        j['starttime'], '%Y-%m-%dT%H:%M:%S'))
                e = calendar.timegm(time.strptime(
                        j['endtime'], '%Y-%m-%dT%H:%M:%S'))
                # if match in year before to week after event
                if year_before <= s and week_after >= s:
                    total += e - s
                    if total >= 6*604800 and i not in matches:
                        matches.append(i)
    return matches


# If avaible, get ROA(s) for prefix
def get_roa(prefix):
    """If available, get ROA(s) for  given prefix

    Parameters:
        prefix (string): IP prefix

    Returns:
        list: list containing ROAs if available, otherwise empty list
    """

    roas = []
    # Get supernets for prefix
    ip = IPNetwork(prefix)
    supernets = ip.supernet(1)

    if prefix in data.rpki.keys():
        roa = data.rpki[prefix]
        for r in roa:
            roas.append(r)
    else:
        for s in supernets:
            if str(s) in data.rpki.keys():
                roa = data.rpki[str(s)]
                for r in roa:
                    if r['maxLength'] >= ip.prefixlen:
                        roas.append(r)
    return roas


# Check if prefix was in use during 6 months before timestamp
def check_prefix_in_use(timestamp, prefix, exp_prefix):
    """Check if prefix was in use for more than 6 weeks during the 6 months
    before the given timestamp

    Parameters:
        timestamp (int): start time in epoch format
        prefix (string): IP prefix
        exp_prefix (string): IP prefix

    Returns:
        bool: True is prefix was in use, False if not
    """

    if prefix in config.prefix_history.keys():
        prefix_timelines = config.prefix_history[prefix]
    else:
        hist = cd.get_prefix_routing_history(prefix)
        if hist is not None:
            config.prefix_history[prefix] = hist['data']['by_origin']
            prefix_timelines = hist['data']['by_origin']
        if hist is None:
            prefix_timelines = None

    six_months = 6*2629743
    six_weeks = 6*604800
    for i in prefix_timelines:
        for j in i['prefixes']:
            if j['prefix'] == prefix or j['prefix'] == exp_prefix:
                timelines = j['timelines']
                duration = 0
                for k in timelines:
                    # Turn timestamp into required format
                    s = calendar.timegm(time.strptime(
                        k['starttime'], '%Y-%m-%dT%H:%M:%S'))
                    e = calendar.timegm(time.strptime(
                        k['endtime'], '%Y-%m-%dT%H:%M:%S'))
                    # If timeline in six months before timestamp
                    if (s >= timestamp-six_months):
                        duration += e - s
                if duration >= six_weeks:
                    return True
    return False


# Get prefix status from RSEFdata
def get_prefix_status(prefix):
    """Get prefix status from RSEF data

    Parameters:
        prefix (string): IP prefix

    Returns:
        tuple: (registry, status, date)
    """
    ip = IPNetwork(prefix)
    supernets = ip.supernet(4)
    supernets.append(ip)
    status = ''
    registry = ''
    date = ''
    for i in supernets:
        network = str(i)
        pr = network.split('/')[0]  # Prefix only
        if ip.version == 4:  # IPv4
            length = 2**(32-int(network.split('/')[1]))  # Size of block
        else:  # IPv6
            length = int(network.split('/')[1])  # CIDR prefix length
        for listing in data.rsef:
            entries = listing[listing['start'] == pr]
            entries = entries[entries['value'] == length]
            if len(entries) > 0:
                status = entries['status'].iloc[0]
                registry = entries['registry'].iloc[0]
                date = entries['date'].iloc[0]
                return (registry, status, date)
    return (registry, status, date)


# Check if prefix is or was on blacklist after hijack
def check_blacklist(timestamp, prefix):
    """Get blacklist data for prefix after the given timestamp

    Parameters:
        timestamp(int): start time in epoch format
        prefix (string): IP prefix

    Returns:
        dict: blacklist data
    """

    if prefix in config.blacklist_data.keys():
        blacklist_timelines = config.blacklist_data[prefix]
    else:
        blacklist = cd.get_prefix_blacklist_data(prefix)
        if blacklist is not None:
            config.blacklist_data[prefix] = blacklist['data']['sources']
            blacklist_timelines = blacklist['data']['sources']
        if blacklist is None:
            blacklist_timelines = None

    blacklisted = dict()
    for source in blacklist_timelines:
        times_blacklisted = 0
        duration_blacklist = 0
        blacklisted_subnets = dict()
        blacklisted_supernets = dict()
        for entry in blacklist_timelines[source]:
            # Check if entry prefix is a valid prefix
            try:
                IPNetwork(entry['prefix'])
            except Exception:
                continue
            if entry['prefix'] == prefix:
                timelines = entry['timelines']
                for i in timelines:
                    # Turn timestamp into required format
                    s = calendar.timegm(time.strptime(
                        i['starttime'], '%Y-%m-%dT%H:%M:%S'))
                    e = calendar.timegm(time.strptime(
                        i['endtime'], '%Y-%m-%dT%H:%M:%S'))
                    # If timeline starts after hijack
                    if s >= timestamp:
                        times_blacklisted += 1
                        duration_blacklist += e - s
            elif check_if_subnet(entry['prefix'], prefix):
                if entry['prefix'] in blacklisted_subnets.keys():
                    times = blacklisted_subnets[
                                entry['prefix']]['times blacklisted']
                    duration = blacklisted_subnets[
                                entry['prefix']]['duration blacklist']
                else:
                    blacklisted_subnets[entry['prefix']] = dict()
                    times = 0
                    duration = 0
                timelines = entry['timelines']
                for i in timelines:
                    # Turn timestamp into required format
                    s = calendar.timegm(time.strptime(
                        i['starttime'], '%Y-%m-%dT%H:%M:%S'))
                    e = calendar.timegm(time.strptime(
                        i['endtime'], '%Y-%m-%dT%H:%M:%S'))
                    # If timeline starts after hijack
                    if s >= timestamp:
                        times += 1
                        duration += e - s
                blacklisted_subnets[entry['prefix']][
                        'times blacklisted'] = times
                blacklisted_subnets[entry['prefix']][
                        'duration blacklist'] = duration
            elif check_if_subnet(prefix, entry['prefix']):
                if entry['prefix'] in blacklisted_supernets.keys():
                    times = blacklisted_supernets[
                                entry['prefix']]['times blacklisted']
                    duration = blacklisted_supernets[
                                entry['prefix']]['duration blacklist']
                else:
                    blacklisted_supernets[entry['prefix']] = dict()
                    times = 0
                    duration = 0
                timelines = entry['timelines']
                for i in timelines:
                    # Turn timestamp into required format
                    s = calendar.timegm(time.strptime(
                        i['starttime'], '%Y-%m-%dT%H:%M:%S'))
                    e = calendar.timegm(time.strptime(
                        i['endtime'], '%Y-%m-%dT%H:%M:%S'))
                    # If timeline starts after hijack
                    if s >= timestamp:
                        times += 1
                        duration += e - s
                blacklisted_supernets[entry['prefix']][
                        'times blacklisted'] = times
                blacklisted_supernets[entry['prefix']][
                        'duration blacklist'] = duration

        blacklisted[source] = {'blacklisted timelines': blacklist_timelines,
                               'times blacklisted': times_blacklisted,
                               'duration blacklist': duration_blacklist,
                               'blacklisted subnets': blacklisted_subnets,
                               'blacklisted supernets': blacklisted_supernets}
    return blacklisted
