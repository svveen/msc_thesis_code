from collections import Counter
import pandas as pd

from plotly.offline import plot
import plotly.graph_objects as go


# By AS
def get_groups_by_as(hijack_dataset):
    group_id_detected = hijack_dataset['group ID: detected ASN']
    group_id_expected = hijack_dataset['group ID: expected ASN']

    count_det = Counter(group_id_detected)
    count_exp = Counter(group_id_expected)

    group_size_detected = Counter(count_det.values())
    group_size_expected = Counter(count_exp.values())

    # Graph distribution detected AS group size
    x = list(group_size_detected.keys())
    y = list(group_size_detected.values())

    fig = go.Figure(data=[go.Bar(x=x, y=y)])
    fig.update_xaxes(title_text='group size', titlefont_size=18, dtick=5)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(title_text='Distribution of group size - detected AS',
                      title_font_size=18, xaxis_tickfont_size=18,
                      yaxis_tickfont_size=18)
    plot(fig, auto_open=True)

    # Graph distribution expected AS group size
    x = list(group_size_expected.keys())
    y = list(group_size_expected.values())

    fig = go.Figure(data=[go.Bar(x=x, y=y)])
    fig.update_xaxes(title_text='group size', titlefont_size=18, dtick=5)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(title_text='Distribution of group size - expected AS',
                      title_font_size=18, xaxis_tickfont_size=18,
                      yaxis_tickfont_size=18)
    plot(fig, auto_open=True)

    # Get group IDs for groups with size > ...
    group_ids_det = []
    for i in count_det:
        if count_det[i] > 25:
            group_ids_det.append(i)

    group_ids_exp = []
    for i in count_exp:
        if count_exp[i] > 15:
            group_ids_exp.append(i)

    # Get hijacks in groups
    hijacks_det = hijack_dataset[hijack_dataset[
            'group ID: detected ASN'].isin(
            group_ids_det)]

    hijacks_exp = hijack_dataset[hijack_dataset[
            'group ID: expected ASN'].isin(
            group_ids_exp)]
    return (hijacks_det, hijacks_exp)


# By prefix
def get_groups_by_prefix(hijack_dataset):
    group_id_detected = hijack_dataset['group ID: detected advertisement']
    group_id_expected = hijack_dataset['group ID: expected prefix']

    count_det = Counter(group_id_detected)
    count_exp = Counter(group_id_expected)

    group_size_detected = Counter(count_det.values())
    group_size_expected = Counter(count_exp.values())

    # Graph distribution detected advertisement group size
    x = list(group_size_detected.keys())
    y = list(group_size_detected.values())

    fig = go.Figure(data=[go.Bar(x=x, y=y)])
    fig.update_xaxes(title_text='group size', titlefont_size=18, dtick=1)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(title_text='Distribution of group size - ' +
                      'detected advertisement', title_font_size=18,
                      xaxis_tickfont_size=18, yaxis_tickfont_size=18)
    plot(fig, auto_open=True)

    # Graph distribution expected prefix group size
    x = list(group_size_expected.keys())
    y = list(group_size_expected.values())

    fig = go.Figure(data=[go.Bar(x=x, y=y)])
    fig.update_xaxes(title_text='group size', titlefont_size=18, dtick=5)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(title_text='Distribution of group size - ' +
                      'expected prefix', title_font_size=18,
                      xaxis_tickfont_size=18, yaxis_tickfont_size=18)
    plot(fig, auto_open=True)

    # Get group IDs for groups with size > ...
    group_ids_det = []
    for i in count_det:
        if count_det[i] > 2:
            group_ids_det.append(i)

    group_ids_exp = []
    for i in count_exp:
        if count_exp[i] > 5:
            group_ids_exp.append(i)

    # Get hijacks in groups
    hijacks_det = hijack_dataset[hijack_dataset[
            'group ID: detected advertisement'].isin(
            group_ids_det)]

    hijacks_exp = hijack_dataset[hijack_dataset[
            'group ID: expected prefix'].isin(
            group_ids_exp)]
    return (hijacks_det, hijacks_exp)


# By event
def get_groups_by_event(hijack_dataset):
    group_id = hijack_dataset['group ID: AS + time']
    count = Counter(group_id)
    group_size = Counter(count.values())

    # Graph group size event
    x = list(group_size.keys())
    y = list(group_size.values())

    fig = go.Figure(data=[go.Bar(x=x, y=y)])
    fig.update_xaxes(title_text='group size', titlefont_size=18, dtick=5)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(title_text='Distribution of group size - ' +
                      'detected AS + start time', title_font_size=18,
                      xaxis_tickfont_size=18, yaxis_tickfont_size=18)
    plot(fig, auto_open=True)

    # Get group IDs for groups with size > 5
    group_ids = []
    for i in count:
        if count[i] > 5:
            group_ids.append(i)

    # Get hijacks in groups
    hijacks = hijack_dataset[hijack_dataset[
            'group ID: AS + time'].isin(group_ids)]
    return hijacks


# Same set of detected ASes
def get_groups_by_set(hijack_dataset):
    group_id = hijack_dataset['group ID: detected advertisement set']
    count = Counter(group_id)
    group_size = Counter(count.values())

    # Graph group size event
    x = list(group_size.keys())
    y = list(group_size.values())

    fig = go.Figure(data=[go.Bar(x=x, y=y)])
    fig.update_xaxes(title_text='group size', titlefont_size=18, dtick=3)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(title_text='Distribution of group size - ' +
                      'detected AS set', title_font_size=18,
                      xaxis_tickfont_size=18, yaxis_tickfont_size=18)
    plot(fig, auto_open=True)

    # Get group IDs for groups with size > 1
    group_ids = []
    for i in count:
        if count[i] > 1:
            group_ids.append(i)

    # Get hijacks in groups
    hijacks = hijack_dataset[hijack_dataset[
            'group ID: detected advertisement set'].isin(group_ids)]
    return hijacks


# Detected becomes expected or vice versa
def get_groups_by_switch(hijack_dataset):
    group_id = hijack_dataset['group ID: detected expected switch']
    count = Counter(group_id)
    group_size = Counter(count.values())

    # Graph group size event
    x = list(group_size.keys())
    y = list(group_size.values())

    fig = go.Figure(data=[go.Bar(x=x, y=y)])
    fig.update_xaxes(title_text='group size', titlefont_size=18, dtick=1)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(title_text='Distribution of group size - ' +
                      'detected AS - expected AS switch', title_font_size=18,
                      xaxis_tickfont_size=18, yaxis_tickfont_size=18)
    plot(fig, auto_open=True)

    # Get group IDs for groups with size > 1
    group_ids = []
    for i in count:
        if count[i] > 1:
            group_ids.append(i)

    # Get hijacks in groups
    hijacks = hijack_dataset[hijack_dataset[
            'group ID: detected expected switch'].isin(group_ids)]
    return hijacks


# Analyse hijack groups - AS
def analyse_groups_as(hijack_dataset, hijack_labels):
    by_as = get_groups_by_as(hijack_dataset)

    # Detected advertisement
    detected = by_as[0]
    features = detected[['group ID: detected ASN', 'start time',
                         'detected ASN', 'expected ASN',
                         'detected advertisement', 'expected prefix',
                         'detected organization', 'expected organization',
                         'country detected ASN', 'country expected ASN',
                         'relation detected-expected',
                         'relation detected-expected nm', 'prefix in use',
                         'hijack duration', 'moas', 'blacklist',
                         'rpki detected', 'rpki expected']]
    labels = hijack_labels.iloc[features.index, :]
    l_and_f = pd.concat([labels, features], axis=1)

    ids = list(features['group ID: detected ASN'].unique())
    id_count = Counter(l_and_f['group ID: detected ASN'])
    for i in ids:
        group = l_and_f[l_and_f['group ID: detected ASN'] == i]
        all_features = detected[detected['group ID: detected ASN'] == i]
        subset_features = all_features.iloc[:, 40:50]
        c = list(all_features['country expected ASN'])
        group_count = Counter(features['group ID: detected ASN'])
        count_country = 0
        for i in c:
            if 'US' in str(i):
                count_country += 1

        for j in group.columns:
            print(group[j])

    # Expected prefix
    expected = by_as[1]
    features = expected[['group ID: expected ASN', 'start time',
                         'detected ASN', 'expected ASN',
                         'detected advertisement', 'expected prefix',
                         'detected organization', 'expected organization',
                         'country detected ASN', 'country expected ASN',
                         'relation detected-expected',
                         'relation detected-expected nm', 'prefix in use',
                         'hijack duration', 'moas', 'blacklist',
                         'rpki detected', 'rpki expected']]
    labels = hijack_labels.iloc[features.index, :]
    l_and_f = pd.concat([labels, features], axis=1)

    ids = list(features['group ID: expected ASN'].unique())
    id_count = Counter(l_and_f['group ID: expected ASN'])
    for i in ids:
        group = features[features['group ID: expected ASN'] == i]
        for j in group.columns:
            print(group[j])


# Analyse hijack groups - prefix
def analyse_groups_prefix(hijack_dataset, hijack_labels):
    by_prefix = get_groups_by_prefix(hijack_dataset)

    # Detected advertisement
    detected = by_prefix[0]
    features = detected[['group ID: detected advertisement', 'start time',
                         'detected ASN', 'expected ASN',
                         'detected advertisement', 'expected prefix',
                         'detected organization', 'expected organization',
                         'country detected ASN', 'country expected ASN',
                         'relation detected-expected',
                         'relation detected-expected nm', 'prefix in use',
                         'hijack duration', 'moas', 'blacklist',
                         'rpki detected', 'rpki expected']]
    labels = hijack_labels.iloc[features.index, :]
    l_and_f = pd.concat([labels, features], axis=1)

    ids = list(features['group ID: detected advertisement'].unique())
    id_count = Counter(l_and_f['group ID: detected advertisement'])
    for i in ids:
        group = l_and_f[l_and_f['group ID: detected advertisement'] == i]
        for j in group.columns:
            print(group[j])

    # Expected prefix
    expected = by_prefix[1]
    features = expected[['group ID: expected prefix', 'start time',
                         'detected ASN', 'expected ASN',
                         'detected advertisement', 'expected prefix',
                         'detected organization', 'expected organization',
                         'country detected ASN', 'country expected ASN',
                         'relation detected-expected',
                         'relation detected-expected nm', 'prefix in use',
                         'det in customer cone of exp',
                         'det in customer cone of exp nm',
                         'exp in customer cone of det',
                         'exp in customer cone of det nm', 'moas', 'blacklist',
                         'rpki detected', 'rpki expected']]
    labels = hijack_labels.iloc[features.index, :]
    l_and_f = pd.concat([labels, features], axis=1)

    ids = list(features['group ID: expected prefix'].unique())
    id_count = Counter(l_and_f['group ID: expected prefix'])
    for i in ids:
        group = l_and_f[l_and_f['group ID: expected prefix'] == i]
        for j in group.columns:
            print(group[j])


# Analyse hijack groups - event
def analyse_groups_event(hijack_dataset, hijack_labels):
    by_event = get_groups_by_event(hijack_dataset)
    features = by_event[['group ID: AS + time', 'start time',
                         'detected ASN', 'expected ASN', 'detected AS Path',
                         'path relations',
                         'detected advertisement', 'expected prefix',
                         'country detected ASN', 'country expected ASN',
                         'relation detected-expected',
                         'relation detected-expected nm',
                         'exp in customer cone of det',
                         'det in customer cone of exp', 'prefix in use',
                         'hijack duration', 'moas']]
    labels = hijack_labels.iloc[features.index, :]
    l_and_f = pd.concat([labels, features], axis=1)

    ids = list(features['group ID: AS + time'].unique())
    id_count = Counter(l_and_f['group ID: AS + time'])
    for i in ids:
        group = l_and_f[l_and_f['group ID: AS + time'] == i]
        c = Counter(group['expected ASN'])
        for j in range(0, len(group)):
            row = group.iloc[j, :]
            print(row['expected ASN'] + ' & ' +
                  row['detected advertisement'] + ' & ' +
                  row['expected prefix'] + '& ' +
                  row['detected AS Path'] + ' & ' +
                  row['path relations'] + ' \\\\')


# Analyse hijack groups - detected AS set
def analyse_groups_set(hijack_dataset, hijack_labels):
    by_set = get_groups_by_set(hijack_dataset)
    features = by_set[['group ID: detected advertisement set', 'start time',
                       'detected ASN', 'expected ASN',
                       'detected advertisement']]
    labels = hijack_labels.iloc[features.index, :]
    l_and_f = pd.concat([labels, features], axis=1)

    ids = list(l_and_f['group ID: detected advertisement set'].unique())
    id_count = Counter(l_and_f['group ID: detected advertisement set'])
    for i in ids:
        group = l_and_f[l_and_f['group ID: detected advertisement set'] == i]
        f = features[features['group ID: detected advertisement set'] == i]
        header = ' & '.join(list(f.columns)) + ' \\\\ \\hline'
        print(header)
        for j in range(0, len(f)):
            row = f.iloc[j, :]
            print(' & '.join(map(str, list(row))) + ' \\\\')
        print()


# Analyse hijack groups - detected AS switches to expected AS
def analyse_groups_switch(hijack_dataset, hijack_labels):
    by_switch = get_groups_by_switch(hijack_dataset)

    features = by_switch[['event', 'group ID: detected expected switch',
                          'start time', 'detected ASN', 'expected ASN',
                          'blacklist', 'hijack duration', 'moas',
                          'rpki detected', 'rpki expected', ]]
    labels = hijack_labels.iloc[features.index, :]
    l_and_f = pd.concat([labels, features], axis=1)

    ids = list(l_and_f['group ID: detected expected switch'].unique())
    for i in ids:
        # Create latex table
        group = l_and_f[l_and_f['group ID: detected expected switch'] == i]
        header = ' & '.join(list(group.columns)) + ' \\\\ \\hline'
        print(header)
        for j in range(0, len(group)):
            row = group.iloc[j].copy()
            row['hijack duration'] = int(round(max(
                    row['hijack duration'])/86400, 0))
            moas = []
            for k in row['moas']:
                moas.append(k[0])
            row['moas'] = ' '.join(moas)
            rpki_det = []
            for k in row['rpki detected']:
                rpki_det.append(k['asn'])
            row['rpki detected'] = ' '.join(rpki_det)
            rpki_exp = []
            for k in row['rpki expected']:
                rpki_exp.append(k['asn'])
            row['rpki expected'] = ' '.join(rpki_exp)
            print(' & '.join(map(str, list(row))) + ' \\\\')
        print()


# Give hijacks labels based on the relations found between hijacks
def relation_labels(hijack_dataset, hijack_labels):
    hijack_labels['detected advertisement set - labels'] = None
    hijack_labels['event - labels'] = None
    hijack_labels['det as - labels'] = None
    hijack_labels['exp prefix - labels'] = None

    group = hijack_dataset[
        hijack_dataset['group ID: detected advertisement set'] == 2447]
    hijack_labels.loc[
        group.index, 'detected advertisement set - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: detected advertisement set'] == 2505]
    hijack_labels.loc[
        group.index, 'detected advertisement set - labels'] = 'path hijack'

    group = hijack_dataset[
        hijack_dataset['group ID: detected advertisement set'] == 2502]
    hijack_labels.loc[
        group.index, 'detected advertisement set - labels'] = 'path hijack'

    group = hijack_dataset[
        hijack_dataset['group ID: detected advertisement set'] == 2442]
    hijack_labels.loc[
        group.index, 'detected advertisement set - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: detected advertisement set'] == 2475]
    group = group[group['continuous AS path'] == False]
    hijack_labels.loc[
        group.index, 'detected advertisement set - labels'] = 'path hijack'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 737]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'route leak with re-origination'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 1253]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 364]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'misconfiguration'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 1268]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 1048]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 1400]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 719]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 202]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 675]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 1718]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'path hijack'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 1667]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 1606]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'misconfiguration'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 805]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'path hijack'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 852]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'path hijack'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 366]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'misconfiguration'

    group = hijack_dataset[
        hijack_dataset['group ID: AS + time'] == 1000]
    hijack_labels.loc[
        group.index, 'event - labels'] = 'path hijack'

    group = hijack_dataset[
        hijack_dataset['group ID: detected ASN'] == 92]
    hijack_labels.loc[
        group.index, 'det as - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: detected ASN'] == 703]
    hijack_labels.loc[
        group.index, 'det as - labels'] = 'intentional'

    group = hijack_dataset[
        hijack_dataset['group ID: expected prefix'] == 55]
    hijack_labels.loc[
        group.index, 'exp prefix - labels'] = 'legitimate announcement'

    group = hijack_dataset[
        hijack_dataset['group ID: expected prefix'] == 28]
    hijack_labels.loc[
        group.index, 'exp prefix - labels'] = 'legitimate announcement'

    group = hijack_dataset[
        hijack_dataset['group ID: expected prefix'] == 14]
    hijack_labels.loc[
        group.index, 'exp prefix - labels'] = 'legitimate announcement'

    group = hijack_dataset[
        hijack_dataset['group ID: expected prefix'] == 128]
    hijack_labels.loc[
        group.index, 'exp prefix - labels'] = 'legitimate announcement'

    group = hijack_dataset[
        hijack_dataset['group ID: expected prefix'] == 125]
    hijack_labels.loc[
        group.index, 'exp prefix - labels'] = 'legitimate announcement'

    group = hijack_dataset[
        hijack_dataset['group ID: expected prefix'] == 26]
    hijack_labels.loc[
        group.index, 'exp prefix - labels'] = 'legitimate announcement'

    group = hijack_dataset[
        hijack_dataset['group ID: expected prefix'] == 10]
    hijack_labels.loc[
        group.index, 'exp prefix - labels'] = 'legitimate announcement'

    group = hijack_dataset[
        hijack_dataset['group ID: expected prefix'] == 6]
    hijack_labels.loc[
        group.index, 'exp prefix - labels'] = 'legitimate announcement'

    return hijack_labels
