import os
import calendar
import time
import json as js

import config


# Set history start and history end boundaries for AS and prefix history
def set_history_start_and_end():
    """Set history start and history end boundaries for AS and prefix history

    History start is a year before the first event, history end is a week
    after the last event

    Returns:
        list: history start in yyyy-mm-ddTHH:MM, history start in epoch format,
              history end in yyyy-mm-ddTHH:MM, history end in epoch format
    """

    path = config.data_directory.joinpath('events')
    event_files = os.listdir(path)
    first_event_file = path.joinpath(event_files[0])
    last_event_file = path.joinpath(event_files.pop())

    # Start
    with open(first_event_file) as json_data:
        event_data = js.load(json_data)
    history_start_epoch = calendar.timegm(time.strptime(  # Year before event
                event_data['start time'], '%Y-%m-%d %H:%M:%S UTC')) - 31556926
    history_start = time.strftime('%Y-%m-%dT%H:%M', time.gmtime(
            history_start_epoch))

    # End
    with open(last_event_file) as json_data:
        event_data = js.load(json_data)
    history_end_epoch = calendar.timegm(time.strptime(  # Week after last event
                event_data['start time'], '%Y-%m-%d %H:%M:%S UTC')) + 604800
    history_end = time.strftime('%Y-%m-%dT%H:%M', time.gmtime(
            history_end_epoch))
    return [history_start, history_start_epoch, history_end, history_end_epoch]


# Set history start and end
history_boundaries = set_history_start_and_end()
