from collections import Counter
import pandas as pd


# Analyse invalid hijacks
def analyse_invalid_hijacks(hijack_dataset, hijack_labels):
    events = hijack_labels[hijack_labels['invalid hijack'] == True]
    hijacks = hijack_dataset.iloc[events.index, :]
    features = hijacks[['detected ASN', 'expected ASN', 'detected AS Path',
                        'detected ASN status', 'expected ASN status']]

    print(str(len(events)) + " hijacks have the label 'invalid hijack' This " +
          "is " + str(round(len(events)/len(hijack_dataset)*100, 2)) + "%.")

    for i in range(0, len(features)):
        h = features.iloc[i]
        # Invalid ASN
        if (h['detected ASN status'][1] == 'invalid' or
                h['expected ASN status'][1] == 'invalid'):
            print("Invalid ASN:")
            print(h['detected ASN'] + ' or ' + h['expected ASN'])
        # Inconsistent path
        elif not h['detected AS Path'].endswith('detected ASN'):
            print("Inconsistent path")
            print(h['detected ASN'] + ': ' + h['detected AS Path'])


# Analyse legitimate announcements
def analyse_legitimate(hijack_dataset, hijack_labels):
    events = hijack_labels[hijack_labels['likely legitimate'] == True]
    hijacks = hijack_dataset.iloc[events.index, :]
    features = hijacks[['detected ASN', 'rpki detected', 'rpki expected',
                        'group ID: detected expected switch']]

    print(str(len(events)) + " hijacks have the label  " +
          "'legitimate announcement' This is " +
          str(round(len(events)/len(hijack_dataset)*100, 2)) + "%.")

    roa_detected = Counter(events['ROA for detected ASN'])[True]
    switch = 0
    count = Counter(hijack_dataset['group ID: detected expected switch'])

    for i in range(0, len(features)):
        h = features.iloc[i]
        if count[h['group ID: detected expected switch']] > 1:
            switch += 1

    print(str(roa_detected) + ' hijacks have a ROA for the detected AS')

    failure = hijack_labels[hijack_labels['failure to summarize'] ==
                            'legitimate announcement']
    relation = hijack_labels[hijack_labels['exp prefix - labels'] ==
                             'legitimate announcement']
    all_legit = pd.concat([events, failure, relation])
    print(str(len(failure)) + ' hijacks that are legitimate aggregations')
    print('In total there are ' + str(len(all_legit)) +
          ' legitimate announcements found. This is ' +
          str(len(all_legit)/len(hijack_dataset)*100) + '%.')


# Analyse hijacks of 1 IP address
def analyse_one_address_hijacks(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['1 IP address'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN'])

    print(str(len(labels)) + " hijacks have the label '1 IP address hijack'" +
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) +
          "%.")


# Analyse hijacks of customer route
def analyse_customer_route(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['hijacking customer route'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN'])

    moas_exp = Counter(l_and_f['MOAS expected AS'])
    c = Counter(labels['hijack duration'])

    print(str(len(labels)) +
          " hijacks have the label 'hijacking customer route'" +
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) +
          "%.")


# Analyse hijacks of unused prefixess
def analyse_unused_prefix(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['prefix not in use'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN'])

    leg = l_and_f[l_and_f['likely legitimate']]
    c = Counter(labels['hijack duration'])

    print(str(len(labels)) + " hijacks of a prefix not in use. " +
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) +
          "%.")


# Analyse hijacks of with reserved or unallocated ASes
def analyse_unallocated_reserved_AS(hijack_dataset, hijack_labels):
    # Detected AS
    labels = hijack_labels[hijack_labels['detected ASN status'] !=
                           'allocated by RIR before hijack']
    labels = labels[labels['invalid hijack'] == 0]
    features = hijack_dataset.iloc[labels.index, :]
    features = features[['start time', 'detected ASN', 'expected ASN',
                         'detected AS Path', 'closest prefix detected AS',
                         'path relations', 'path relations nm',
                         'detected advertisement',
                         'detected by x BGPMon peers']]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(features['detected ASN'])

    res_IANA = l_and_f[l_and_f['detected ASN status'] == 'reserved by IANA']
    res_RIR = l_and_f[l_and_f['detected ASN status'] == 'reserved by RIR']
    available = l_and_f[l_and_f['detected ASN status'] == 'available by RIR']
    after = l_and_f[l_and_f['detected ASN status'] ==
                    'allocated by RIR after hijack']
    feat = hijack_dataset.iloc[after.index, :]
    on = l_and_f[l_and_f['detected ASN status'] ==
                    'allocated by RIR on day of hijack']
    unall_IANA = l_and_f[l_and_f['detected ASN status'] ==
                    'unallocated by IANA']
    
    
    for i in range(0, len(unall_IANA)):
        row = unall_IANA.iloc[i]
        print(row['start time'] + ' & ' + row['detected ASN'] + ' & ' +
              row['detected AS Path'] + ' & ' + row['path relations'] + ' & ' +
              str(row['detected by x BGPMon peers']) + ' \\\\')
    
    print(str(len(labels)) +
          " hijacks have a detected AS that should not be used. " +
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) +
          "%.")
    
    # Expected AS
    labels = hijack_labels[hijack_labels['expected ASN status'] !=
                           'allocated by RIR before hijack']
    labels = labels[labels['invalid hijack'] != True]
    features = hijack_dataset.iloc[labels.index, :]
    features = features[['start time', 'detected ASN', 'expected ASN',
                         'detected AS Path', 'closest prefix detected AS',
                         'path relations', 'path relations nm'
                         ]]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN'])
    status = Counter(l_and_f['expected ASN status'])

    res_IANA = l_and_f[l_and_f['expected ASN status'] == 'reserved by IANA']
    res_RIR = l_and_f[l_and_f['expected ASN status'] == 'reserved by RIR']
    expected_AS_count = Counter(res_RIR['expected ASN'])
    available = l_and_f[l_and_f['expected ASN status'] == 'available by RIR']
    features = hijack_dataset.iloc[available.index, :]
    after = l_and_f[l_and_f['expected ASN status'] ==
                    'allocated by RIR after hijack']
    features = hijack_dataset.iloc[after.index, :]

    print(str(len(labels)) +
          " hijacks have a expected AS that should not be used. " +
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) +
          "%.")


# Analyse hijacks of with patterned prefix
def analyse_patterned_prefix(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['detected advertisement pattern'] ==
                           True]
    features = hijack_dataset.iloc[labels.index, :]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN'])
    
    test = l_and_f[l_and_f['likely legitimate']]
    c = Counter(labels['hijack duration'])

    print(str(len(labels)) + " hijacks have a patterned prefix. " + 
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) + 
          "%.")


# Analyse hijacks with AS23456
def analyse_transition_AS(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['AS23456'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN'])


# Analyse hijacks with AS path prepended by origin
def analyse_prepending(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['prepended by origin'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN'])


# Analyse hijacks with failure to aggregate or failure to summarize
def analyse_failures(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['failure to aggregate'] == True]
    
    labels = hijack_labels[hijack_labels['failure to summarize'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    l_and_f = pd.concat([labels, features], axis=1)   


# Analyse wrong prefix length
def analyse_wrong_length(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['wrong prefix length'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    l_and_f = pd.concat([labels, features], axis=1)

    print(str(len(labels)) + " hijacks have a wrong prefix length. " +
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) +
          "%.")

    for i in range(0, len(features)):
        h = features.iloc[i]
        print(str(h['closest prefix detected AS'][1][0]) + ' & ' +
              str(h['detected advertisement']) + ' \\\\')


# Analyse typo
def analyse_typo(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['possible typo'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN'])

    print(str(len(labels)) + " hijacks have a possible typo. " +
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) +
          "%.")

    for i in range(0, len(features)):
        h = features.iloc[i]
        print(str(h['closest prefix detected AS'][1][0]) + ' & ' +
              str(h['detected advertisement']) + ' \\\\')


# Analyse announcing subnet
def analyse_announcing_subnet(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['announcing subnet'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    features = features[['start time', 'detected ASN', 'expected ASN',
                         'detected advertisement', 'expected prefix',
                         'closest prefix detected AS',
                         'relation detected-expected',
                         'relation detected-expected nm',
                         'exp in customer cone of det',
                         'exp in customer cone of det nm']]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN'])
    moas_count = Counter(labels['MOAS expected AS'])
    glob_count = Counter(labels['global hijack'])
    before = l_and_f[l_and_f['announced before']]

    print(str(len(labels)) + " hijacks are announcing subnet. " +
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) +
          "%.")


# Analyse announcing supernet
def analyse_announcing_supernet(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['announcing supernet'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    features = features[['start time', 'detected ASN', 'expected ASN',
                         'detected advertisement', 'expected prefix',
                         'closest prefix detected AS',
                         'relation detected-expected',
                         'relation detected-expected nm',
                         'exp in customer cone of det',
                         'exp in customer cone of det nm']]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN'])
    moas_count = Counter(labels['MOAS expected AS'])
    glob_count = Counter(labels['global hijack'])

    print(str(len(labels)) + " hijacks are announcing supernet. " +
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) +
          "%.")


# Analyse no continuous path
def analyse_no_continuous_path(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['no continuous path'] == True]
    features = hijack_dataset.iloc[labels.index, :]
    features = features[['start time', 'detected ASN', 'expected ASN',
                         'detected AS Path', 'closest prefix detected AS',
                         'path relations', 'path relations nm'
                         ]]
    l_and_f = pd.concat([labels, features], axis=1)
    duration = Counter(labels['hijack duration'])
    glob = Counter(labels['global hijack'])
    no_roa_all = Counter(hijack_labels['no ROA'])
    no_roa = Counter(labels['no ROA'])

    print(str(len(labels)) + " hijacks without continuous path. " +
          "This is " + str(round(len(labels)/len(hijack_dataset)*100, 2)) +
          "%.")


# Analyse blacklisted hijacks
def analyse_blacklist(hijack_dataset, hijack_labels):
    # Before hijack
    det_bef = hijack_labels[hijack_labels['detected blacklist before'] == True]
    exp_bef = hijack_labels[hijack_labels['expected blacklist before'] == True]
    sub_bef = hijack_labels[hijack_labels['subnet blacklist before'] == True]
    sub_features = hijack_dataset.iloc[sub_bef.index, :]
    sub_features = sub_features[['detected ASN', 'expected ASN',
                                 'detected advertisement', 'expected prefix',
                                 'detected AS Path', 'path relations']]
    sub_all = pd.concat
    sup_bef = hijack_labels[hijack_labels['supernet blacklist before'] == True]
    concat = pd.concat([det_bef, exp_bef, sub_bef, sup_bef], axis=0)
    c = Counter(concat['event number'])

    print('There are ' + str(len(c)) + ' hijacks with a prefix blacklisted ' +
          'before the start of the hijack. This is ' +
          str(len(c)/len(hijack_dataset)*100) + '%')

    # After hijack
    det_aft = hijack_labels[hijack_labels['detected blacklist after'] == True]
    exp_aft = hijack_labels[hijack_labels['expected blacklist after'] == True]
    sub_aft = hijack_labels[hijack_labels['subnet blacklist after'] == True]
    sup_aft = hijack_labels[hijack_labels['supernet blacklist after'] == True]
    concat = pd.concat([det_aft, exp_aft, sub_aft, sup_aft], axis=0)
    c = Counter(sub_aft['hijack duration'])

    print('There are ' + str(len(c)) + ' hijacks with a prefix blacklisted ' +
          'during the hijack. This is ' +
          str(len(c)/len(hijack_dataset)*100) + '%')


# Analyse ROAs
def analyse_roa(hijack_dataset, hijack_labels):
    no_roa = hijack_labels[hijack_labels['no ROA'] == True]
    roa_detected = hijack_labels[hijack_labels['ROA for detected ASN'] == True]
    features = hijack_dataset.iloc[roa_detected.index, :]
    roa_expected = hijack_labels[hijack_labels['ROA for expected ASN'] == True]
    roa_other = hijack_labels[hijack_labels['ROA for other ASN'] == True]
    roa_other = roa_other[roa_other['ROA for expected ASN'] == False]
    other = Counter(roa_other['ROA for expected ASN'])
    expected = Counter(roa_expected['no continuous path'])

    print('There are ' + str(len(no_roa)) + ' hijacks without a ROA. This is ' +
          str(len(no_roa)/len(hijack_dataset)*100) + '%')

# Analyse global vs local
def analyse_global_local(hijack_dataset, hijack_labels):
    global_hijacks =  hijack_labels[hijack_labels['global hijack'] == 1]
    local_hijacks =  hijack_labels[hijack_labels['global hijack'] == 0]
    glob_count = Counter(global_hijacks['detected advertisement set - labels'])
    loc_count = Counter(local_hijacks['detected advertisement set - labels'])


# Analyse MOAS conflicts
def analyse_MOAS(hijack_dataset, hijack_labels):
    moas_feature = hijack_dataset['moas']
    moas_amounts = []
    indices = []

    for i in range(0, len(moas_feature)):
        moas_amounts.append(len(moas_feature.iloc[i]))
        if len(moas_feature.iloc[i]) > 3:
            indices.append(i)

    count_amounts = Counter(moas_amounts)
    large_conflict = hijack_labels.iloc[indices, :]
    features = hijack_dataset.iloc[indices, :]

    moas_other = hijack_labels[hijack_labels['MOAS expected AS'] == 0]
    c = Counter(moas_other['MOAS related to detected'])


# Analyse special and reserved prefixes
def analyse_special_prefixes(hijack_dataset, hijack_labels):
    status = Counter(hijack_labels['detected advertisement status'])
    available = hijack_labels[hijack_labels['detected advertisement status']
                    == 'prefix available or without status']
    after = hijack_labels[hijack_labels['detected advertisement status']
                    == 'prefix allocated after hijack']
    reserved = hijack_labels[hijack_labels['detected advertisement status']
                    == 'prefix reserved']
    special = hijack_labels[hijack_labels['special prefix hijacked'] == 1]
    features = hijack_dataset.iloc[special.index, :]


# Analyse announced before by detected AS
def analyse_announced_before(hijack_dataset, hijack_labels):
    labels = hijack_labels[hijack_labels['announced before'] == 1]
    features = hijack_dataset.iloc[labels.index, :]
    features = features[['start time', 'detected ASN', 'expected ASN',
                         'detected advertisement', 'expected prefix',
                         'closest prefix detected AS',
                         'relation detected-expected',
                         'relation detected-expected nm',
                         'exp in customer cone of det',
                         'exp in customer cone of det nm']]
    l_and_f = pd.concat([labels, features], axis=1)
    detected_AS_count = Counter(l_and_f['detected ASN']) 

    print('There are ' + str(len(labels)) + ' hijacks of a prefix that was ' + 
          'announced before by the detected AS. This is ' +
          str(len(labels)/len(hijack_dataset)*100) + '%')

    legitimate = Counter(labels['likely legitimate'])
    summarize = Counter(labels['failure to summarize'])

    count = labels[labels['failure to summarize'] != 'legitimate announcement']
    roa = Counter(count['ROA for detected ASN'])
    count = count[count['ROA for detected ASN'] == 0]
    exp_prefix_labels = Counter(count['exp prefix - labels'])
    duration = Counter(labels['hijack duration'])
    all_duration = Counter(hijack_labels['hijack duration'])


# Analyse duration
def analyse_hijack_duration(hijack_dataset, hijack_labels):
    less_than_day = hijack_labels[hijack_labels['hijack duration'] ==
                                  'less than a day']
    more_than_month = hijack_labels[hijack_labels['hijack duration'] ==
                                  'more than a month']
    up_to_week = hijack_labels[hijack_labels['hijack duration'] ==
                                  'up to a week']
    up_to_month = hijack_labels[hijack_labels['hijack duration'] ==
                                  'up to a month']
