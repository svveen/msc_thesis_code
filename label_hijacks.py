import calendar
import time

from netaddr import IPNetwork
import pandas as pd

import asn_attributes as aa
import event_attributes as ea
import prefix_attributes as pa
import labelling_functions as lf


# Collect and compute all necessary attributes of a hijack
def get_hijack_attributes(hijack):
    """Collect and compute all necessary attributes of a hijack

    Parameters:
        hijack (dict): hijack from BGPStream

    Returns:
        dict: hijack attributes

    """

    next_month = ea.get_next_month(hijack['start time'])

    hijack['timestamp'] = calendar.timegm(time.strptime(
            hijack['start time'], '%Y-%m-%d %H:%M:%S UTC'))

    hijack['IP version'] = IPNetwork(hijack['detected advertisement']).version

    hijack['detected by x BGPMon peers'] = \
        int(hijack['detected by x BGPMon peers'])

    hijack['relation detected-expected'] = aa.get_relation_ASes(
            hijack['start time'], hijack['detected ASN'],
            hijack['expected ASN'])

    hijack['relation detected-expected nm'] = aa.get_relation_ASes(
            next_month, hijack['detected ASN'],
            hijack['expected ASN'])

    hijack['size customer cone detected ASN'] = aa.get_size_AS_customer_cone(
            hijack['start time'], hijack['detected ASN'])

    hijack['size customer cone expected ASN'] = aa.get_size_AS_customer_cone(
            hijack['start time'], hijack['expected ASN'])

    hijack['detected ASN status'] = aa.check_ASN_status(
            hijack['timestamp'], hijack['detected ASN'])

    hijack['expected ASN status'] = aa.check_ASN_status(
            hijack['timestamp'], hijack['expected ASN'])

    hijack['closest prefix detected AS'] = aa.get_closest_prefix(
            hijack['detected ASN'], hijack['detected advertisement'],
            hijack['timestamp'])

    hijack['closest prefix expected AS'] = aa.get_closest_prefix(
            hijack['expected ASN'], hijack['detected advertisement'],
            hijack['timestamp'])

    if hijack['closest prefix detected AS'] is None:  # No history available
        hijack['difference prefix length'] = None
    else:
        hijack['difference prefix length'] = aa.get_difference_prefix_length(
                hijack['detected advertisement'],
                hijack['closest prefix detected AS'][1])

    hijack['detected advertisement subnet of expected'] = pa.check_if_subnet(
            hijack['detected advertisement'], hijack['expected prefix'])

    hijack['announcing subnet'] = pa.check_announcing_subnet(
            hijack['detected ASN'], hijack['detected advertisement'],
            hijack['timestamp'])

    hijack['announcing supernet'] = pa.check_announcing_supernet(
            hijack['detected ASN'], hijack['detected advertisement'],
            hijack['timestamp'])

    hijack['moas'] = ea.check_MOAS_conflict(
            hijack['detected advertisement'], hijack['timestamp'])

    hijack['special prefix'] = pa.check_special_prefixes(
            hijack['detected advertisement'])

    hijack['AS23456'] = ea.check_as23456(hijack['detected AS Path'])

    hijack['country detected ASN'] = aa.get_AS_country(hijack['detected ASN'])

    hijack['country expected ASN'] = aa.get_AS_country(hijack['expected ASN'])

    hijack['country of hijacked prefix'] = pa.get_prefix_country(
                                            hijack['detected advertisement'])

    hijack['path prepending'] = ea.check_path_prepending(
            hijack['detected AS Path'], hijack['detected ASN'])

    hijack['exp in customer cone of det'] = aa.check_if_as1_in_cc_of_as2(
            hijack['start time'], hijack['expected ASN'],
            hijack['detected ASN'])

    hijack['exp in customer cone of det nm'] = aa.check_if_as1_in_cc_of_as2(
        next_month, hijack['expected ASN'],
        hijack['detected ASN'])

    hijack['det in customer cone of exp'] = aa.check_if_as1_in_cc_of_as2(
            hijack['start time'], hijack['detected ASN'],
            hijack['expected ASN'])

    hijack['det in customer cone of exp nm'] = aa.check_if_as1_in_cc_of_as2(
            next_month, hijack['detected ASN'],
            hijack['expected ASN'])

    hijack['path relations'] = ea.get_as_path_relations(
            hijack['start time'], hijack['detected AS Path'])

    hijack['path relations nm'] = ea.get_as_path_relations(
        next_month, hijack['detected AS Path'])

    hijack['continuous AS path'] = ea.check_continuous_path(
            hijack['path relations'], hijack['path relations nm'])

    if hijack['continuous AS path'] is True:
        result = []
        result.append(ea.check_valley_free_path(hijack['path relations']))
        result.append(ea.check_valley_free_path(hijack['path relations nm']))
        if True in result:
            hijack['valley free path'] = True
        else:
            hijack['valley free path'] = False
    else:
        hijack['valley free path'] = False

    hijack['path length'] = len(hijack['detected AS Path'].split(' '))

    hijack['rpki detected'] = pa.get_roa(hijack['detected advertisement'])
    hijack['rpki expected'] = pa.get_roa(hijack['expected prefix'])

    hijack['hijack duration'] = ea.get_hijack_duration(
            hijack['timestamp'], hijack['detected ASN'],
            hijack['detected advertisement'])

    hijack['prefix in use'] = pa.check_prefix_in_use(
            hijack['timestamp'], hijack['detected advertisement'],
            hijack['expected prefix'])

    hijack['detected advertisement status'] = pa.get_prefix_status(
            hijack['detected advertisement'])

    hijack['expected prefix status'] = pa.get_prefix_status(
            hijack['expected prefix'])

    hijack['detected AS organization ID'] = aa.get_AS_organization(
            hijack['timestamp'], hijack['detected ASN'])

    hijack['expected AS organization ID'] = aa.get_AS_organization(
            hijack['timestamp'], hijack['expected ASN'])

    hijack['blacklist'] = pa.check_blacklist(
            hijack['timestamp'], hijack['detected advertisement'])

    return hijack


# Label hijacks
def get_hijack_labels(hijack_dataset):
    """Label hijacks

    Parameters:
        hijack_dataset (DataFrame): hijack_dataset with group IDs

    Returns:
        DataFrame: event numbers with labels
    """

    labels = pd.DataFrame(columns=['event number', 'invalid hijack',
                                   'likely legitimate', '1 IP address',
                                   'prefix not in use',
                                   'detected blacklist after',
                                   'expected blacklist after',
                                   'supernet blacklist after',
                                   'subnet blacklist after',
                                   'detected blacklist before',
                                   'expected blacklist before',
                                   'supernet blacklist before',
                                   'subnet blacklist before',
                                   'detected ASN status',
                                   'expected ASN status',
                                   'detected advertisement status',
                                   'detected advertisement pattern',
                                   'MOAS expected AS',
                                   'MOAS unrelated AS',
                                   'MOAS related to expected',
                                   'MOAS related to detected',
                                   'MOAS related to expected and detected',
                                   'no continuous path',
                                   'special prefix hijacked',
                                   'hijacking customer route', 'global hijack',
                                   'announced before', 'AS23456',
                                   'wrong prefix length',
                                   'prepended by origin', 'no ROA',
                                   'ROA for detected ASN',
                                   'ROA for expected ASN', 'ROA for other ASN',
                                   'announcing subnet', 'announcing supernet',
                                   'failure to summarize',
                                   'failure to aggregate'])

    for i in range(0, len(hijack_dataset)):
        print(i)
        hijack = hijack_dataset.iloc[i]
        labeled = {'event number': hijack['event']}

        # Find invalid hijacks
        labeled['invalid hijack'] = lf.label_invalid_hijack(hijack)
        if labeled['invalid hijack']:  # do not further label invalid hijacks
            labels = labels.append(pd.Series(labeled), ignore_index=True)
            continue

        # Find likely legitimate announcements
        labeled['likely legitimate'] = lf.label_likely_legitimate(
                hijack_dataset, hijack)

        # Hijack of 1 ip address
        labeled['1 IP address'] = IPNetwork(hijack[
                'detected advertisement']).size == 1

        # Hijcking unused prefix
        labeled['prefix not in use'] = not hijack['prefix in use']

        # Blacklist labels
        blacklist_labels = lf.label_blacklisted(hijack)
        labeled['detected blacklist after'] = blacklist_labels[0]
        labeled['expected blacklist after'] = blacklist_labels[1]
        labeled['supernet blacklist after'] = blacklist_labels[2]
        labeled['subnet blacklist after'] = blacklist_labels[3]
        labeled['detected blacklist before'] = blacklist_labels[4]
        labeled['expected blacklist before'] = blacklist_labels[5]
        labeled['supernet blacklist before'] = blacklist_labels[6]
        labeled['subnet blacklist before'] = blacklist_labels[7]

        # Status detected origin AS and expected AS
        labeled['detected ASN status'] = lf.label_AS_status(
                hijack['detected ASN status'], hijack['start time'])
        labeled['expected ASN status'] = lf.label_AS_status(
                hijack['expected ASN status'], hijack['start time'])

        # Status detected advertisement
        labeled['detected advertisement status'] = \
            lf.label_prefix_status(hijack)

        # Detected advertisement pattern
        labeled['detected advertisement pattern'] = \
            lf.label_patterned_prefix(hijack)

        # MOAS conflicts
        MOAS_labels = lf.label_moas_conflicts(hijack)
        labeled['MOAS expected AS'] = 'expected AS' in MOAS_labels
        labeled['MOAS unrelated AS'] = 'unrelated' in MOAS_labels
        labeled['MOAS related to expected'] = \
            'related to expected' in MOAS_labels
        labeled['MOAS related to detected'] = \
            'related to detected' in MOAS_labels
        labeled['MOAS related to expected and detected'] = \
            'related to expected and detected' in MOAS_labels

        # No continuous path
        labeled['no continuous path'] = not hijack['continuous AS path']

        # Hijack of special prefix
        labeled['special prefix hijacked'] = hijack['special prefix']

        # Hijacking customer route
        labeled['hijacking customer route'] = lf.label_customer_route(hijack)

        # Global hijack
        labeled['global hijack'] = \
            hijack['detected advertisement subnet of expected']

        # Detected AS announced detected advertisement before
        labeled['announced before'] = lf.label_announced_before(hijack)

        # AS23456
        labeled['AS23456'] = hijack['AS23456']

        # Wrong prefix length
        labeled['wrong prefix length'] = lf.label_wrong_length(hijack)

        # Hijack duration
        labeled['hijack duration'] = lf.label_hijack_duration(hijack)

        # Path prepended by origin AS
        labeled['prepended by origin'] = hijack['path prepending'] > 1

        # RPKI labels
        RPKI_labels = lf.label_rpki(hijack)
        labeled['no ROA'] = RPKI_labels[0]
        labeled['ROA for detected ASN'] = RPKI_labels[1]
        labeled['ROA for expected ASN'] = RPKI_labels[2]
        labeled['ROA for other ASN'] = RPKI_labels[3]

        # Announcing subnet
        labeled['announcing subnet'] = hijack['announcing subnet'] != []

        # Announcing supernet
        labeled['announcing supernet'] = hijack['announcing supernet'] != []

        # Failure to summarize
        labeled['failure to summarize'] = lf.label_failure_to_summarize(hijack)

        # Failure to aggregate
        labeled['failure to aggregate'] = lf.label_failure_to_aggregate(hijack)

        # Possible typo
        labeled['possible typo'] = lf.label_possible_typo(hijack)

        labels = labels.append(pd.Series(labeled), ignore_index=True)

    return labels
