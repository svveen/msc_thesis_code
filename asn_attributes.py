import calendar
import time
import math
import difflib
from collections import Counter

import stringdist
import pandas as pd
from netaddr import IPNetwork

import collect_data as cd
import data
import config


# Get the relationship between two ASes
def get_relation_ASes(starttime, as1, as2):
    """Get the relationship between two ASes

    Get relationship between two ASes from CAIDA data. Data file used is
    the one from the beginning of the month of the specified start time

    Parameters:
        starttime (string): YYYY-mm-dd HH:MM:SS UTC
        as1 (string/int): AS number
        as2 (string/int): AS number

    Returns:
        string: relation as1 to as2 or 'no relation' if no relation
    """

    # First from starttime find which relation files should be used
    start = starttime.split(' ')[0].replace('-', '')
    files = list(data.AS_relationship_data.keys())
    index1 = '-'
    index2 = '-'
    for i in files:  # Find file from month in which event occurs
        if i.startswith(start[0:6]) and i.endswith('as-rel.txt'):
            index1 = files.index(i)
        elif i.startswith(start[0:6]) and i.endswith('as-rel2.txt'):
            index2 = files.index(i)

    as1 = int(as1)
    as2 = int(as2)
    rel1 = data.AS_relationship_data[files[index1]]
    rel2 = data.AS_relationship_data[files[index2]]

    # Find relation
    r1 = rel1.loc[(rel1.AS_x == as1) | (rel1.AS_y == as1)]
    r1 = r1.loc[(r1.AS_x == as2) | (r1.AS_y == as2)]
    r2 = rel2.loc[(rel2.AS_x == as1) | (rel2.AS_y == as1)]
    r2 = r2.loc[(r2.AS_x == as2) | (r2.AS_y == as2)]
    relationships = pd.merge(r1, r2, on=['AS_x', 'AS_y', 'relationship'])

    if len(relationships) == 0:
        return 'no relation'
    else:
        if (relationships['relationship'] == 0).bool():
            return 'p2p'
        elif (relationships['relationship'] == -1).bool():
            if as1 == relationships['AS_x'][0]:
                return 'p2c'
            else:
                return 'c2p'


# Get size of customer cone of given AS
def get_size_AS_customer_cone(starttime, asn):
    """Get size of customer cone of given AS

    Get size of customer cone of AS from CAIDA data. Data file used is
    the one from the beginning of the month of the specified start time

    Parameters:
        starttime (string): YYYY-mm-dd HH:MM:SS UTC
        asn (string/int): AS number

    Returns:
        int: size of customer cone
    """

    # First from starttime find which customer cone file should be used
    start = starttime.split(' ')[0].replace('-', '')
    files = list(data.AS_relationship_data.keys())
    index = '-'
    for i in files:  # Find file from month in which event occurs
        if i.startswith(start[0:6]) and i.endswith('ppdc-ases.txt'):
            index = files.index(i)
            break

    cc = data.AS_relationship_data[files[index]]
    cc = cc.loc[cc.cone_AS == str(asn)]
    if len(cc) == 0:
        return 0
    # Return #customers-1 because they list themselves as customer
    return len(cc['customers'].iloc[0].split(' '))-1


# Check if AS1 is in customer cone of AS2
def check_if_as1_in_cc_of_as2(starttime, as1, as2):
    """Check if AS1 is in customer cone of AS2

    Check if AS1 is in customer cone of AS2 using CAIDA data. Data file used is
    the one from the beginning of the month of the specified start time.

    Parameters:
        starttime (string): YYYY-mm-dd HH:MM:SS UTC
        as1 (string/int): AS number
        as2 (string/int): AS number

    Returns:
        bool: True if AS1 is in customer cone of AS2, False otherwise

    """

    # First from starttime find which customer cone file should be used
    start = starttime.split(' ')[0].replace('-', '')
    files = list(data.AS_relationship_data.keys())
    index = '-'
    for i in files:  # Find file from month in which event occurs
        if i.startswith(start[0:6]) and i.endswith('ppdc-ases.txt'):
            index = files.index(i)

    cc = data.AS_relationship_data[files[index]]
    cc = cc.loc[cc.cone_AS == str(as2)]
    if len(cc) > 0:
        customers = cc['customers'].iloc[0].replace('\n', '').split(' ')
        return str(as1) in customers
    else:
        return False


# Check if AS is a reserved AS or unallocated AS at given time
def check_ASN_status(timestamp, asn):
    """Check if AS is a reserved AS or unallocated AS at given time

    Parameters:
        timestamp (int): time in epoch format
        asn (string/int): AS number

    Returns:
        tuple: First element is IANA in case of reserved or unallocated asn,
              or registry in case of assigned asn.
              Second element is state of AS at given time
              (reserved, unallocated, assigned, allocated, or availble)
    """

    asn = int(asn)
    reserved_ASes = [0, 23456, 65535, 4294967295]
    # Invalid ASes
    if (asn < 0) or (asn > 4294967295):
        return('IANA', 'invalid', '-')
    # Reserved ASes
    elif ((asn in reserved_ASes) or
            (asn >= 64496 and asn <= 131071) or
            (asn >= 4200000000 and asn <= 4294967294)):
        return ('IANA', 'reserved', '-')
    # Unallocated ASes
    elif ((asn >= 139578 and asn <= 196607) or
          (asn >= 207260 and asn <= 210331 and timestamp <= 1534723200) or
          (asn >= 210332 and asn <= 262143) or
          (asn >= 268701 and asn <= 270748 and timestamp <= 1544140800) or
          (asn >= 270749 and asn <= 327679) or
          (asn >= 328704 and asn <= 393215) or
          (asn >= 397213 and asn <= 399260 and timestamp <= 1533772800) or
          (asn >= 399261 and asn <= 4199999999)):
        return ('IANA', 'unallocated', '-')
    else:
        asn = str(asn)
        # Find status in RSEF files
        status = ''
        for listing in data.rsef:
            entries = listing[listing['type'] == 'asn']
            match = entries[entries['start'] == asn]
            if len(match) == 1:  # AS is in listing
                status = match['status'].iloc[0]
                registry = match['registry'].iloc[0]
                date = match['date'].iloc[0]
                return (registry, status, date)
            else:  # AS might be in an assigned block
                blocks = entries[(entries['value'] > 1)]
                for i in range(0, len(blocks)):
                    entry = blocks.iloc[i]
                    s = int(entry['start'])
                    if s < int(asn) and int(asn) < (s + int(entry['value'])):
                        # AS is in block
                        status = entry['status']
                        registry = entry['registry']
                        date = entry['date']
                        return (registry, status, date)
    return ('IANA', 'assigned', '-')


# Get the country that occurs most in list of countries
def get_most_common_country(country_list):
    """Get country that occurs most often in list

    Parameters:
        country_list (list): list of countries found in geolite/ip2lite

    Returns:
        list: list with country that occurs the most. In case two or more
              countries occur the same amount of times, all are in this list
    """

    if country_list is None or country_list == []:
        return None

    # GeoLite
    if type(country_list[0]) == dict:
        countries = []
        for i in country_list:
            countries.append(str(i['geoname_id']))
            countries.append(str(i['registered_country_geoname_id']))
        country_list = countries

    keep_count = dict(Counter(country_list))
    if 'nan' in keep_count.keys():  # ensure not only nan is given (geolite)
        keep_count['nan'] = -keep_count['nan']
    if '-' in keep_count.keys():  # ensure not only - is given (ip2lite)
        keep_count['-'] = -keep_count['-']

    if len(keep_count) == 0:
        country = None
    else:
        country = []
        max_occurence = max(keep_count.values())
        for i in keep_count:
            # Get list of countries with max occurence
            if abs(keep_count[i]) == abs(max_occurence):
                country.append(i)
    return country


# Get AS country from RSEF files
def get_AS_country_rsef(asn):
    """Get AS country from RSEF files

    Parameters:
        asn (string/int): AS number

    Returns:
        string: country code
    """

    asn = str(asn)
    # Find country in RSEF files
    country_rsef = ''
    for listing in data.rsef:
        entries = listing[listing['type'] == 'asn']
        match = entries[entries['start'] == asn]
        if len(match) == 1:  # AS is in listing
            country_rsef = match['cc'].iloc[0]
        else:  # AS might be in an assigned block
            blocks = entries[(entries['value'] > 1)]
            for i in range(0, len(blocks)):
                entry = blocks.iloc[i]
                s = int(entry['start'])
                if s < int(asn) and int(asn) < (s + int(entry['value'])):
                    # AS is in block
                    country_rsef = entry['cc']
    return country_rsef


# Get the country in which AS is located from GeoLite2
def get_AS_country_geolite(asn):
    """Get AS country from GeoLite2 data

    Parameters:
        asn (string/int): AS number

    Returns:
        list: list with country for each of the prefixed owned by AS
    """

    asn = int(asn)
    ip_asn = data.geolite[0]
    ip_country = data.geolite[1]

    # Find networks for asn
    if asn in ip_asn.keys():
        asn_networks = ip_asn[asn].copy()
    else:
        return []
    country_list = []
    # Get all geoname_ids for found networks
    for i in asn_networks:
        if i in ip_country.keys():
            country_list.append(ip_country[i])
        else:
            ip = IPNetwork(i)
            supernets = ip.supernet(4)
            for j in supernets:
                if str(j) in ip_country.keys():
                    country_list.append(ip_country[str(j)])
                    break

    return country_list


# Get AS country from IP2Location LITE data
def get_AS_country_ip2lite(asn):
    """Get AS country from IP2Location LITE data

    Parameters:
        asn (string/int): AS number

    Returns:
        list: list with country for each of the prefixed owned by AS
    """

    asn = str(asn)
    # Get all prefixes for asn
    as_data_ipv4 = data.ip2lite[0]
    as_data_ipv6 = data.ip2lite[1]
    prefixes_ipv4 = as_data_ipv4.loc[as_data_ipv4[3] == asn]
    prefixes_ipv6 = as_data_ipv6.loc[as_data_ipv6[3] == asn]
    # Find country per IP
    country_data_ipv4 = data.ip2lite[2]
    country_data_ipv6 = data.ip2lite[3]
    country_list = []
    for i in range(0, len(prefixes_ipv4)):
        entries = country_data_ipv4.loc[
                country_data_ipv4[0] <= prefixes_ipv4.iloc[i][1]]
        entries = entries.loc[entries[1] >= prefixes_ipv4.iloc[i][0]]
        for j in range(0, len(entries)):
            country_list.append(entries.iloc[j][2])

    prefixes_ipv6[0] = prefixes_ipv6[0].map(int)
    prefixes_ipv6[1] = prefixes_ipv6[1].map(int)
    for i in range(0, len(prefixes_ipv6)):
        entries = country_data_ipv6.loc[
                country_data_ipv6[0].map(int) <= prefixes_ipv6.iloc[i][1]]
        entries = entries.loc[entries[1].map(int) >= prefixes_ipv6.iloc[i][0]]
        for j in range(0, len(entries)):
            country_list.append(entries.iloc[j][2])
    return country_list


# For given AS, get AS country from geolite, rsef and ip2lite
def get_AS_country(asn):
    """Get AS country from GeoLite2, RSEF lists and IP2Location LITE

    Parameters:
        asn (string/int): AS number

    Returns:
        list: List with most occuring country for AS found for each dataset.
             List has the following order: GeoLite2, RSEF, IP2Location LITE.
    """

    geolite_country = get_most_common_country(get_AS_country_geolite(asn))
    rsef_country = get_AS_country_rsef(asn)
    ip2lite_country = get_most_common_country(get_AS_country_ip2lite(asn))
    return (geolite_country, rsef_country, ip2lite_country)


# Get string distance and closest prefix from normally anounced prefixes
def get_closest_prefix(asn, prefix, timestamp):
    """From history of AS, get prefix with lowest distance to specified prefix.
    Closest prefix has to be announced for a total duration of at least six
    weeks. Returns string distance and closest prefix.

    Parameters:
        asn (string/int): AS number
        prefix (string): IP prefix
        timestamp (int): event start time converted to epoch

    Returns:
        tuple (int, string): distance between prefixes and closest prefix
    """

    if asn in config.AS_history.keys():
        timelines = config.AS_history[asn]
        if timelines is None:
            return None
    else:
        hist = cd.get_AS_routing_history(asn)
        if hist is not None:
            timelines = []
            for i in hist:
                for j in i['data']['prefixes']:
                    timelines.append(j)
            config.AS_history[asn] = timelines
        if hist is None:
            config.AS_history[asn] = None
            return None
    matching_prefixes = []  # List of prefixes that are normally announced
    year_before = timestamp - 31556926
    week_after = timestamp + 604800
    for i in timelines:
        # Check if prefix was announced for longer period of time
        t = i['timelines']
        total = 0
        for j in t:
            # Turn timestamp into required format
            s = calendar.timegm(time.strptime(
                j['starttime'], '%Y-%m-%dT%H:%M:%S'))
            e = calendar.timegm(time.strptime(
                j['endtime'], '%Y-%m-%dT%H:%M:%S'))
            # if match in year before to week after event, append
            if year_before <= s and week_after >= s:
                total += e - s
                # Check if this prefix is normally announced, meaning for at
                # least six weeks at a time or in total over multiple periods
                if total >= 6*604800 and i['prefix'] not in matching_prefixes:
                    matching_prefixes.append(i['prefix'])

    # Find the closest prefix in the normally announced prefixes
    matching_prefixes_no_length = []
    for i in matching_prefixes:
        matching_prefixes_no_length.append(i.split('/')[0])
    matches = difflib.get_close_matches(prefix.split('/')[0],
                                        matching_prefixes_no_length,
                                        n=20, cutoff=0.1)
    lowest_distance = math.inf
    closest_prefix = []
    p1 = prefix.split('/')[0]
    for m in matches:
        p2 = m
        ndif = stringdist.rdlevenshtein(p1, p2)
        if ndif < lowest_distance:
            lowest_distance = ndif
            # Get closest prefix with length from matching_prefixes
            closest_prefix = []
            for i in matching_prefixes:
                if i.startswith(m):
                    closest_prefix.append(i)
        elif ndif == lowest_distance:
            for i in matching_prefixes:
                if i.startswith(m) and i not in closest_prefix:
                    closest_prefix.append(i)

    return(lowest_distance, closest_prefix)


# Get smallest difference in prefix length
def get_difference_prefix_length(p1, prefix_list):
    """For a given prefix and a list of prefixes, find the closest distance in
    prefix lengths between given prefix and prefix from the list

    Parameters:
        p1 (string): IP prefix
        prefix_list (list): list of IP prefixes

    Returns:
        int: positive difference means given prefix is more specific, negative
             difference means prefix from list is more specific
    """

    p1v = IPNetwork(p1).version
    p1_length = p1.split('/')[1]
    smallest_diff = math.inf
    for p2 in prefix_list:
        if p1 == p2:
            return 0
        # Check if both are same version
        p2v = IPNetwork(p2).version
        if p1v != p2v:
            continue
        p2_length = p2.split('/')[1]
        diff = int(p1_length)-(int(p2_length))
        if abs(diff) < abs(smallest_diff):
            smallest_diff = diff
    return smallest_diff


# Get AS organization around time of hijack
def get_AS_organization(starttime, asn):
    """Get AS organization from CAIDA around time of hijack

    Parameters:
        starttime (string): hijack start time
        asn (int/string): AS number

    Returns:
        string: organization ID
    """

    # First from starttime find which file should be used
    start = int(starttime.split(' ')[0].replace('-', ''))
    files = list(data.AS_organization_data.keys())
    index = '-'
    dif = math.inf
    for i in files:  # Find file from month in which event occurs
        file_date = int(i[0:8])
        if abs(start-file_date) < dif:
            dif = abs(start-file_date)
            index = files.index(i)

    org = data.AS_organization_data[files[index]]
    org = org.loc[org['aut/org_id'] == str(asn)]

    if len(org) == 1:
        return org.iloc[0]['org_id/country']
    elif len(org) == 0:
        return None
    else:
        ids = list(org['org_id/country'].unique())
        if len(ids) > 1:
            return ids
        else:
            return ids[0]
