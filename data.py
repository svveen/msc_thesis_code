import collect_data as cd

# Load datasets
AS_relationship_data = cd.load_AS_relationship_data()
AS_organization_data = cd.load_AS_organization_data()
geolite = cd.load_geolite_data()
ip2lite = cd.load_ip2lite_data()
rsef = cd.get_RSEF_listings()
rpki = cd.get_rpki_info()

# Load BGPStream events
events = cd.load_BGPStream_events()
bgpstream_hijacks = events[0]
