import calendar
import time
import math
from collections import Counter

from netaddr import IPNetwork

import prefix_attributes as pa
import asn_attributes as aa
import event_attributes as ea
import collect_data as cd


# Check if hijack is valid
def label_invalid_hijack(hijack):
    """Check if hijack is valid

    Parameters:
        hijack (Series): row from hijack_dataset

    Returns:
        bool: True if the hijack is invalid, false otherwise
    """

    # Detected ASN does not match origin in AS path
    if hijack['path prepending'] == 0:
        return True
    # Detected ASN is invalid
    elif hijack['detected ASN status'] == ('IANA', 'invalid', '-'):
        return True
    # Expected ASN is invalid
    elif hijack['expected ASN status'] == ('IANA', 'invalid', '-'):
        return True
    else:
        return False


# Check if hijack can be legitimate announcement
def label_likely_legitimate(hijack_dataset, hijack):
    """Check if hijack is likely a legitimate announcement

    Parameters:
        hijack_dataset (DataFrame): data set of grouped hijacks
        hijack (Series): row from hijack_dataset

    Returns:
        bool: True is likely legitimate, false otherwise
    """

    # Check RPKI
    if hijack['rpki detected'] != []:
        roas = hijack['rpki detected']
        detected_AS = 'AS' + hijack['detected ASN']
        for roa in roas:
            if roa['asn'] == detected_AS:
                return True

    # Check if detected AS becomes expected AS
    id_name = 'group ID: detected expected switch'
    group_id_count = Counter(hijack_dataset[id_name])
    group_id = hijack[id_name]
    if group_id_count[group_id] > 1:
        group = hijack_dataset[hijack_dataset[id_name] == group_id]
        # Check if detected AS was detected and expected
        if (hijack['detected ASN'] in list(group['detected ASN']) and
                hijack['detected ASN'] in list(group['expected ASN'])):
            expected = group[group['expected ASN'] == hijack['detected ASN']]
            # Check if detected ASN is later the expected ASN
            for i in range(0, len(expected)):
                if hijack['timestamp'] < expected.iloc[i]['timestamp']:
                    return True
    return False


# Function helping label_blacklisted()
def blacklist_count_after(counts, timestamp, hijack_duration):
    if counts['count after'] > 0:
        if (counts['first time after'][0] <=
                (timestamp + max(hijack_duration))):
            return True
    return False


# Function helping label_blacklisted()
def blacklist_count_before(counts, timestamp, hijack_duration):
    if counts['count before'] > 0:
        if counts['last time before'][1] >= timestamp:
            return True
    return False


# Find if prefix is blacklisted due to hijack
def label_blacklisted(hijack):
    """Check if hijack caused blacklisting of the prefix

    Parameters:
        hijack (Series): row from hijack_dataset

    Returns:
        list: list of bools corresponding to labels
    """

    if hijack['blacklist'] == {}:
        return [False]*8

    blacklist = hijack['blacklist']
    detected_prefix = hijack['detected advertisement']
    expected_prefix = hijack['expected prefix']
    hijack_duration = hijack['hijack duration']
    timestamp = hijack['timestamp']

    counts = {'count before': 0, 'count after': 0,
              'last time before': (-math.inf, -math.inf),
              'first time after': (math.inf, math.inf)}
    type_counts = {'detected prefix': counts.copy(),
                   'expected prefix': counts.copy(), 'subnets': counts.copy(),
                   'supernets': counts.copy()}

    # Count all times supernets, subnets, expected prefix and detected prefix
    # Are blacklisted before and after hijack
    for source in blacklist:
        source_data = blacklist[source]
        timelines = source_data['blacklisted timelines'][source]
        for i in timelines:
            times = i['timelines']
            # Find if timelines is from detected, supernet or subnet
            if i['prefix'] == detected_prefix:
                prefix_type = 'detected prefix'
            elif i['prefix'] == expected_prefix:
                prefix_type = 'expected prefix'
            elif pa.check_if_subnet(i['prefix'], detected_prefix):
                prefix_type = 'subnets'
            else:
                prefix_type = 'supernets'

            for j in times:
                s = calendar.timegm(time.strptime(
                        j['starttime'], '%Y-%m-%dT%H:%M:%S'))
                e = calendar.timegm(time.strptime(
                        j['endtime'], '%Y-%m-%dT%H:%M:%S'))
                if s < timestamp:
                    type_counts[prefix_type]['count before'] += 1
                else:
                    type_counts[prefix_type]['count after'] += 1
                # Find first time blacklisted after hijack
                if (s >= timestamp and
                        s < type_counts[prefix_type]['first time after'][0]):
                    type_counts[prefix_type]['first time after'] = (s, e)
                # Find last time blacklisted before hijack
                if (s < timestamp and
                        s > type_counts[prefix_type]['last time before'][0]):
                    type_counts[prefix_type]['last time before'] = (s, e)

    # Process counts and return labels
    labels = []
    detected = type_counts['detected prefix']
    expected = type_counts['expected prefix']
    supernet = type_counts['supernets']
    subnet = type_counts['subnets']
    # Blacklisted after hijack
    labels.append(blacklist_count_after(detected, timestamp, hijack_duration))
    labels.append(blacklist_count_after(expected, timestamp, hijack_duration))
    labels.append(blacklist_count_after(supernet, timestamp, hijack_duration))
    labels.append(blacklist_count_after(subnet, timestamp, hijack_duration))
    # Blacklisted before hijack
    labels.append(blacklist_count_before(detected, timestamp, hijack_duration))
    labels.append(blacklist_count_before(expected, timestamp, hijack_duration))
    labels.append(blacklist_count_before(supernet, timestamp, hijack_duration))
    labels.append(blacklist_count_before(subnet, timestamp, hijack_duration))
    return labels


# Find status of involved ASes
def label_AS_status(AS_status, starttime):
    """Get labels related to the status of the detected origin AS and the
    expected AS

    Parameters:
        AS_status (tuple): ASN status from hijack
        starttime (string): start time from hijack

    Returns:
        tuple: labels for detected and expected AS as string
    """

    starttime = int(starttime.split(' ')[0].replace('-', ''))
    if AS_status[0] == 'IANA':
        if AS_status[1] == 'unallocated':
            return 'unallocated by IANA'
        if AS_status[1] == 'reserved':
            return 'reserved by IANA'
    else:
        if AS_status[1] == 'available':
            return 'available by RIR'
        if AS_status[1] == 'reserved':
            return 'reserved by RIR'
        if AS_status[1] == 'assigned' or AS_status[1] == 'allocated':
            if int(AS_status[2]) < starttime:
                return 'allocated by RIR before hijack'
            elif int(AS_status[2]) == starttime:
                return 'allocated by RIR on day of hijack'
            else:
                return 'allocated by RIR after hijack'
    return ''


# Find status of prefixes related to hijack
def label_prefix_status(hijack):
    """Get label for prefix status

    Parameters:
        hijack (Series): hijack from hijack_dataset

    Returns:
        string: label
    """

    status = hijack['detected advertisement status']
    starttime = int(hijack['start time'].split(' ')[0].replace('-', ''))

    if status[1] == 'assigned' or status[1] == 'allocated':
        if starttime > int(status[2]):
            return 'prefix allocated before hijack'
        elif starttime == int(status[2]):
            return 'prefix allocated on day of hijack'
        else:
            return 'prefix allocated after hijack'
    elif status[1] == 'reserved':
        return 'prefix reserved'
    else:
        return 'prefix available or without status'


# Check if the prefix length is wrong (prefix is same as normally announced,
# but prefix length differs)
def label_wrong_length(hijack):
    """Check for a mistake in prefix length. There is a mistake if the prefix
    itself is the same as what is normally announced but the prefix length
    differs.

    Parameters:
        hijack (dict): hijack attributes returned from get_hijack_attributes()

    Returns:
        bool: True if mistake in prefix length, False otherwise
    """
    # Mistake in prefix length only counts if the prefix itself is the same
    if (hijack['closest prefix detected AS'][0] == 0 and
            hijack['difference prefix length'] != 0):
        return True
    else:
        return False


# Check for MOAS conflicts
def label_moas_conflicts(hijack):
    """Check for moas conflicts

    Parameters:
        hijack (dict): hijack attributes returned from get_hijack_attributes()

    Returns:
        list: empty list if no conflicts, otherwise list containing if there
              is a conflict with the expected AS or another AS
    """

    label = []
    exp = False  # Check for conflict with expected AS
    other = []  # Check for conflict with other AS
    detected = hijack['detected ASN']
    expected = hijack['expected ASN']
    if len(hijack['moas']) > 0:
        for i in hijack['moas']:
            if i[0] == detected:
                continue
            if i[0] == expected:
                exp = True
            else:
                other.append(i[0])
    if exp:
        label.append('expected AS')
    if len(other) > 0:
        # Check if other AS is related to detected or expected AS
        starttime = hijack['start time']
        next_month = ea.get_next_month(starttime)
        for i in other:
            do_start = aa.get_relation_ASes(starttime, detected, i)
            eo_start = aa.get_relation_ASes(starttime, expected, i)
            do_next = aa.get_relation_ASes(next_month, detected, i)
            eo_next = aa.get_relation_ASes(next_month, expected, i)
            if do_start == 'no relation' and do_next == 'no relation':
                if eo_start == 'no relation' and eo_next == 'no relation':
                    if 'unrelated' not in label:
                        label.append('unrelated')
                else:
                    if ('related to expected' not in label):
                        label.append('related to expected')
            else:
                if eo_start == 'no relation' and eo_next == 'no relation':
                    if ('related to detected' not in label):
                        label.append('related to detected')
                else:
                    if ('related to detected and expected' not in label):
                        label.append('related to detected and expected')
    return label


# Check for detected advertisements with a strange pattern
def label_patterned_prefix(hijack):
    """Labels hijacks that have a detected advertisement with
    repeating octets/hextets. For example: x.x.x.x or x.x.x.0

    Parameters:
        hijack (dict): hijack attributes returned from get_hijack_attributes()

    Returns:
        bool: True is prefix is patterned, false otherwise
    """

    # Divide prefix into octets/hextets
    prefix = hijack['detected advertisement'].split('/')[0]
    if '.' in prefix:
        prefix = prefix.split('.')
    else:
        prefix = prefix.split(':')
        if len(prefix) <= 3:
            return False

    # Remove doubles
    prefix = list(dict.fromkeys(prefix))

    if len(prefix) <= 2:
        return True
    else:
        return False


# Label hijacking customer route
def label_customer_route(hijack):
    """Labels hijacks that have a detected AS hijacking a route of its
    customer

    Parameters:
        hijack (dict): hijack attributes returned from get_hijack_attributes()

    Returns:
        bool: True if hijacking customer route, False otherwise
    """

    relation = hijack['relation detected-expected']
    relation_nm = hijack['relation detected-expected nm']
    if relation == 'p2c' or relation_nm == 'p2c':
        return True
    else:
        return False


# Label detected AS announced detected advertisement before
def label_announced_before(hijack):
    """Labels hijacks of prefixes that are announced before by the
    detected AS

    Parameters:
        hijack (dict): hijack attributes returned from get_hijack_attributes()

    Returns:
        bool: True is prefix was announced before, False otherwise
    """

    closest = hijack['closest prefix detected AS']
    if closest[0] == 0 and hijack['difference prefix length'] == 0:
        return True
    else:
        return False


# Label hijack duration
def label_hijack_duration(hijack):
    """Labels hijacks based on hijack duration

    Parameters:
        hijack (dict): hijack attributes returned from get_hijack_attributes()

    Returns:
        string: less than a day, up to a week, up to a month, more than a month
    """
    duration = hijack['hijack duration']

    if duration == (0, 0):
        return 'less than a day'
    if max(duration) <= 604800:
        return 'up to a week'
    if max(duration) <= 2629743:
        return 'up to a month'
    else:
        return 'more than a month'


# Get RPKI related labels
def label_rpki(hijack):
    """Check ROAs for hijack and return corresponding labels

    Parameters:
        hijack (dict): hijack attributes returned from get_hijack_attributes()

    Returns:
        list:   List contains booleans for the following labels:
                'no ROA' if no ROA is found
                'ROA for detected ASN' if detected ASN has ROA for
                detected advertisement
                'ROA for expected ASN' if expected ASN has ROA for
                detected advertisement
                'ROA for other ASN' if other ASN has ROA for
                detected advertisement
    """
    detected = 'AS' + hijack['detected ASN']
    expected = 'AS' + hijack['expected ASN']
    rpki_detected = hijack['rpki detected']
    labels = [False, False, False, False]
    if rpki_detected == []:
        return [True, False, False, False]

    for i in rpki_detected:
        if i['asn'] == detected:
            labels[1] = True
        elif i['asn'] == expected:
            labels[2] = True
        else:
            labels[3] = True
    return labels


# Failure to summarize
def label_failure_to_summarize(hijack):
    """Labels hijacks caused by failure to summarize

    Parameters:
        hijack (dict): hijack attributes returned from get_hijack_attributes()

    Returns:
        bool: True if failure to summarize, 'legitimate announcement' if
        subnets cover whole or most of range of supernet, False otherwise
    """

    # AS is announcing supernet instead of the subnets
    supernet = hijack['announcing supernet']
    if supernet == []:
        return False

    # Check if normally announcing multiple subnets
    subnets = []
    for i in supernet:
        if i['prefix'] not in subnets:
            subnets.append(i['prefix'])

    if len(subnets) > 1:
        prefix = hijack['detected advertisement']
        size_supernet = IPNetwork(prefix).size
        size_subnets = 0
        for i in subnets:
            size_subnets += IPNetwork(i).size
        if size_supernet == size_subnets:
            return 'legitimate announcement'
        if size_supernet*0.75 < size_subnets:
            return True
    return False


# Failure to aggregate
def label_failure_to_aggregate(hijack):
    """Labels hijacks caused by failure to aggregate

    Parameters:
        hijack (dict): hijack attributes returned from get_hijack_attributes()

    Returns:
        bool: True if failure to aggregate, False otherwise
    """

    subnet = hijack['announcing subnet']
    if subnet == []:
        return False

    supernets = []
    for i in subnet:
        supernets.append(i['prefix'])

    # Check if AS also announces other subnets
    asn = hijack['detected ASN']
    hist = cd.get_AS_routing_history(asn)
    timelines = []
    for i in hist:
        for j in i['data']['prefixes']:
            timelines.append(j)

    # Check if all subnets of supernet are announced instead of supernet
    timestamp = hijack['timestamp']
    for supernet in supernets:
        subnets = []
        for i in timelines:
            if pa.check_if_subnet(i['prefix'], supernet):
                periods = i['timelines']
                for j in periods:
                    # Turn timestamp into required format
                    s = calendar.timegm(time.strptime(
                        j['starttime'], '%Y-%m-%dT%H:%M:%S'))
                    e = calendar.timegm(time.strptime(
                        j['endtime'], '%Y-%m-%dT%H:%M:%S'))
                    if s <= timestamp and e >= timestamp:
                        if i['prefix'] not in subnets:
                            subnets.append(i['prefix'])
        size_supernet = IPNetwork(supernet).size
        size_subnets = 0
        for s in subnets:
            size_subnets += IPNetwork(s).size
        if size_supernet == size_subnets:
            return True
    return False


# Label hijacks that may be caused by a typographical error in the prefix
def label_possible_typo(hijack):
    """Label hijack that may be caused by a typographical error in the prefix

    Parameters:
        hijack (dict): hijack attributes returned from get_hijack_attributes()

    Returns:
        bool: True if possible typo, False otherwise
    """

    if (hijack['difference prefix length'] == 0 and
            hijack['closest prefix detected AS'][0] == 1):
        return True
    else:
        return False
