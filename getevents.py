# Import modules
from bs4 import BeautifulSoup
import requests
import json
import os
from pathlib import Path


# Download event for given event number
# Input: event number
# Output: False if unable to download event, otherwise event data
def get_BGPStream_event(event_number):
    url = 'https://bgpstream.com/event/' + str(event_number)
    r = requests.get(url)
    if r.status_code == 200:
        return r.text
    else:
        return False


# Save event data to file
def save_BGPStream_event(data, event_number):
    data['event'] = event_number
    filename = (Path(__file__).parent.joinpath('Data/events/' +
                str(event_number) + '.txt'))
    file = open(filename, 'w')
    file.write(json.dumps(data))
    file.close()


# Parse hijack
def BGP_hijack(soup):
    data = soup.find_all('td')
    json = dict()
    json['type'] = 'hijack'
    # Get start_time
    json['start time'] = str(data[0])[16:39]
    # Get expected prefix
    s = str(data[1])
    index_start = s.find(':') + 2
    index_end = s.find('</td>')
    json['expected prefix'] = s[index_start:index_end]
    # Get expected ASN
    s = str(data[2])
    index_start = s.find('ASN') + 5
    index_end = s.find(' ', index_start)
    json['expected ASN'] = s[index_start:index_end]
    # Get expected organization
    index_start = s.find('(') + 1
    index_end = s.find('</') - 1
    json['expected organization'] = s[index_start:index_end]
    # Get detected advertisement
    s = str(data[3])
    index_start = s.find(':') + 2
    index_end = s.find('</td>')
    json['detected advertisement'] = s[index_start:index_end]
    # Get detected ASN
    s = str(data[4])
    index_start = s.find('ASN') + 4
    index_end = s.find(' ', index_start)
    json['detected ASN'] = s[index_start:index_end]
    # Get expected organization
    index_start = s.find('(') + 1
    index_end = s.find('</') - 1
    json['detected organization'] = s[index_start:index_end]
    # Get detected AS Path
    s = str(data[5])
    index_start = s.find('Path') + 5
    index_end = s.find('</td>')
    json['detected AS Path'] = s[index_start:index_end]
    # Get number of peers that detected hijack
    s = str(data[6])
    index_start = s.find(':') + 2
    index_end = s.find('</td>')
    json['detected by x BGPMon peers'] = s[index_start:index_end]
    return json


# Parse route leak
def BGP_leak(soup):
    data = soup.table.find_all('td')
    json = dict()
    json['type'] = 'leak'
    # Get start_time
    json['start time'] = str(data[0])[16:39]
    # Get leaked prefix
    s = str(data[1])
    index_start = s.find(':') + 2
    index_end = s.find('(') - 1
    json['leaked prefix'] = s[index_start:index_end]
    # Get leaked prefix ASN
    index_start = s.find('AS') + 2
    index_end = s.find(' ', index_start)
    json['ASN of leaked prefix'] = s[index_start:index_end]
    # Get leaked prefix organization
    index_start = index_end + 1
    index_end = s.find('</') - 1
    json['organization of leaked prefix'] = s[index_start:index_end]
    # Get leaked by ASN
    s = str(data[2])
    index_start = s.find(':') + 4
    index_end = s.find('<img') - 1
    json['leaked by ASN'] = s[index_start:index_end]
    # Get leaked by organization
    index_start = s.find('(') + 1
    index_end = s.find('</') - 1
    json['leaked by organization'] = s[index_start:index_end]
    # Get leaked to
    s = data[3].find_all('li')
    leaked_to = []
    for i in s:
        li = str(i)
        # Get ASN
        index_start = li.find('>') + 1
        index_end = li.find('(') - 1
        asn = li[index_start:index_end]
        # Get organisation
        index_start = li.find('(') + 1
        index_end = li.find('</') - 1
        org = li[index_start:index_end]
        leaked_to.append((asn, org))
    json['leaked to'] = leaked_to
    # Get number of BGPMon peers that saw it
    s = str(data[5])
    index_start = s.find(':') + 2
    index_end = s.find('</td>')
    json['Seen by x BGPMon peers'] = s[index_start:index_end]
    return json


# Parse outage
def BGP_outage(soup):
    header = str(soup.h3)
    data = soup.table.find_all('td')
    json = dict()
    outage_type = 'country'
    if header.find('ASN') > -1:
        outage_type = 'ASN'
    json['type'] = "outage " + outage_type
    if outage_type == 'ASN':
        # Get ASN
        index_start = header.find('ASN') + 4
        index_end = header.find('(') - 1
        json['ASN'] = header[index_start:index_end]
        # Get AS organization
        index_start = header.find('(') + 1
        index_end = header.find('</') - 7
        json['organization'] = header[index_start:index_end]
    else:
        # Get country
        index_start = header.find('for ') + 4
        index_end = header.find('<img ') - 2
        json['country'] = header[index_start:index_end]
    # Get start time
    json['start time'] = str(data[0])[16:39]
    # Get optional end time
    i = 1
    end_time = ''
    if len(data) == 3:
        end_time = str(data[1])[14:37]
        i = 2
    json['end time'] = end_time
    # Get number of prefixes affected
    s = str(data[i])
    index_start = s.find(':') + 2
    index_end = s.find('(') - 1
    json['number of affected prefixes'] = int(s[index_start:index_end])
    # Get percentage of prefixes affected
    index_start = s.find('(')+1
    index_end = s.find(')')-1
    json['percentage of prefixes affected'] = int(s[index_start:index_end])
    return json


# Get event and turn into json format
def event_to_JSON(event_number):
    html = get_BGPStream_event(event_number)
    if html is False:
        return False
    soup = BeautifulSoup(html, 'html.parser')
    # Find event type
    header = soup.h3
    s = str(header)
    if s.find('outage for') > -1:
        data = BGP_outage(header.parent)
    elif s.find('hijack') > -1:
        data = BGP_hijack(header.parent)
    elif s.find('Leak') > -1:
        data = BGP_leak(header.parent)
    if s != 'None':
        save_BGPStream_event(data, event_number)


# Download events
def get_events(first, last):
    if not os.path.exists(Path(__file__).parent.joinpath('Data/events/')):
        os.makedirs(Path(__file__).parent.joinpath('Data/events/'))
    no_data = []
    for i in range(first, last+1):
        print(i)
        e = event_to_JSON(i)
        if e is False:
            no_data.append(i)
    print(no_data)


get_events(137603, 205530)
