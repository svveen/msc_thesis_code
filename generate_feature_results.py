from collections import Counter
import mpu

from plotly.offline import plot
import plotly.graph_objects as go
import pycountry as pc
import pandas as pd

import config
import data


# IP version distribution
def get_ip_version_distribution(hijack_dataset):
    """Get distribution of IP version

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    expected_distinct_prefixes = list(
            hijack_dataset['expected prefix'].unique())

    exp_ipv4 = 0
    exp_ipv6 = 0
    exp_ipv6_list = []
    for i in expected_distinct_prefixes:
        if ':' in i:
            exp_ipv6 += 1
            exp_ipv6_list.append(i)
        else:
            exp_ipv4 += 1

    p = round(exp_ipv4/len(expected_distinct_prefixes)*100, 2)
    print('There are ' + str(exp_ipv4) + ' unique IPv4 prefixes hijacked and '
          + str(exp_ipv6) + ' IPv6 hijacks. ' + str(p) +
          '% of hijacks are IPv4.')

    ipv = hijack_dataset['IP version']
    ip_count = Counter(ipv)
    p = round(ip_count[4]/len(ipv)*100, 2)
    print('In total there are ' + str(ip_count[4]) + ' IPv4 prefixes and ' +
          str(ip_count[6]) + ' IPv6 prefixes. ' + str(p) + '% is IPv4')


# Ratio of global and local hijacks
def get_ratio_global_local(hijack_dataset):
    """Get ratio of global and local hijacks

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    global_vs_local = Counter(
            hijack_dataset['detected advertisement subnet of expected'])
    p = round(global_vs_local[0.0]/len(hijack_dataset)*100, 2)
    print('There are ' + str(global_vs_local[0.0]) + ' local hijacks and ' +
          str(global_vs_local[1.0]) + ' global hijacks. ' + str(p) +
          '% of hijacks is local.')


# Graph prefix lengths
def graph_prefix_length(hijack_dataset):
    """Make a graph of the prefix lengths of detected advertisements

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    prefixes = hijack_dataset['detected advertisement']
    ipv6_lengths = []
    ipv4_lengths = []
    for i in prefixes:
        if '.' in i:
            ipv4_lengths.append(int(i.split('/')[1]))
        else:
            ipv6_lengths.append(int(i.split('/')[1]))

    # Graph IPv4
    ipv4_counts = Counter(ipv4_lengths)
    x = list(ipv4_counts.keys())
    y = list(ipv4_counts.values())

    fig = go.Figure(data=[go.Bar(x=x, y=y, text=y, textfont_size=18,
                                 textposition='auto')])
    fig.update_xaxes(title_text='prefix length', titlefont_size=18, dtick=1)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(
            title_text='Distribution of IPv4 prefix length in hijacks',
            title_font_size=18, xaxis_tickfont_size=18, yaxis_tickfont_size=18)
    plot(fig, auto_open=True)

    # Graph IPv6
    ipv6_counts = Counter(sorted(ipv6_lengths))
    x = list(ipv6_counts.keys())
    y = list(ipv6_counts.values())

    fig = go.Figure(data=[go.Bar(x=x, y=y, text=y, textfont_size=18,
                                 textposition='auto')])
    fig.update_xaxes(title_text='prefix length', type='category',
                     titlefont_size=18, dtick=1, tickmode='array', tickvals=x)
    fig.update_yaxes(title_text='count', titlefont_size=18)
    fig.update_layout(
            title_text='Distribution of IPv6 prefix length in hijacks',
            title_font_size=18, xaxis_tickfont_size=18, yaxis_tickfont_size=18)
    plot(fig, auto_open=True)


# Graphs for path length and path prepending
def graph_path_length_and_prepending(hijack_dataset):
    """Make graphs for AS path length and path prepending

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    ipv4 = hijack_dataset[hijack_dataset['IP version'] == 4]
    ipv6 = hijack_dataset[hijack_dataset['IP version'] == 6]

    # Graph path length
    path_data_4 = ipv4[['path length', 'path prepending']]
    path_data_6 = ipv6[['path length', 'path prepending']]
    path_data_4 = path_data_4[path_data_4['path prepending'] > 0]  # no invalid
    path_data_6 = path_data_6[path_data_6['path prepending'] > 0]
    path_length_4 = Counter(path_data_4['path length'])
    path_length_6 = Counter(path_data_6['path length'])
    path_prepending_4 = Counter(path_data_4['path prepending'])
    path_prepending_6 = Counter(path_data_6['path prepending'])

    x_4 = list(path_length_4.keys())
    y_4 = list(path_length_4.values())

    x_6 = list(path_length_6.keys())
    y_6 = list(path_length_6.values())

    fig = go.Figure(data=[go.Bar(name='IPv4', x=x_4, y=y_4, text=y_4,
                                 textfont_size=18, textposition='auto'),
                          go.Bar(name='IPv6', x=x_6, y=y_6, text=y_6,
                                 textfont_size=18, textposition='auto')])
    fig.update_xaxes(title_text='path length', titlefont_size=18, dtick=1)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(title_text='Distribution of AS path length',
                      title_font_size=18, xaxis_tickfont_size=18,
                      yaxis_tickfont_size=18)
    plot(fig, auto_open=True)

    # Graph path prepending
    x_4 = list(path_prepending_4.keys())
    y_4 = list(path_prepending_4.values())

    x_6 = list(path_prepending_6.keys())
    y_6 = list(path_prepending_6.values())

    fig = go.Figure(data=[go.Bar(x=x_4, y=y_4, text=y_4, textfont_size=18,
                                 textposition='auto', name='IPv4'),
                          go.Bar(x=x_6, y=y_6, text=y_6, textfont_size=18,
                                 textposition='auto', name='IPv6')])
    fig.update_xaxes(title_text='path prepending', titlefont_size=18, dtick=1)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(title_text='Distribution of AS path prepending',
                      title_font_size=18, xaxis_tickfont_size=18,
                      yaxis_tickfont_size=18)
    plot(fig, auto_open=True)

    # Graph relation
    ipv4_y = list(path_data_4['path length'])
    ipv6_y = list(path_data_6['path length'])
    ipv4_x = list(path_data_4['path prepending'])
    ipv6_x = list(path_data_6['path prepending'])

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=ipv4_x, y=ipv4_y, mode='markers',
                             name='IPv4'))
    fig.add_trace(go.Scatter(x=ipv6_x, y=ipv6_y, mode='markers',
                             name='IPv6'))
    fig.update_xaxes(rangemode='tozero', dtick=1, title_text='Path prepending',
                     titlefont_size=18)
    fig.update_yaxes(title_text='Path length', titlefont_size=18)
    fig.update_layout(title_text='Path prepending and path length',
                      title_font_size=18, xaxis_tickfont_size=18,
                      xaxis_title_font_size=18, yaxis_title_font_size=18,
                      yaxis_tickfont_size=18)
    fig.update_traces(opacity=0.55)
    plot(fig, auto_open=True)

    # Check prepending by other AS
    paths = hijack_dataset[['detected AS Path', 'IP version']]
    paths_ipv4 = paths[paths['IP version'] == 4]
    paths_ipv6 = paths[paths['IP version'] == 6]
    ipv4_lengths = []
    ipv6_lengths = []
    for p in paths_ipv4['detected AS Path']:
        p = list(dict.fromkeys(p.split(' ')))  # remove duplicates
        ipv4_lengths.append(len(p))
    ipv4_lengths = Counter(ipv4_lengths)

    for p in paths_ipv6['detected AS Path']:
        p = list(dict.fromkeys(p.split(' ')))  # remove duplicates
        ipv6_lengths.append(len(p))
    ipv6_lengths = Counter(ipv6_lengths)

    # Graph path length without prepending
    x_4 = list(ipv4_lengths.keys())
    y_4 = list(ipv4_lengths.values())

    x_6 = list(ipv6_lengths.keys())
    y_6 = list(ipv6_lengths.values())

    fig = go.Figure(data=[go.Bar(name='IPv4', x=x_4, y=y_4, text=y_4,
                                 textfont_size=18, textposition='auto'),
                          go.Bar(name='IPv6', x=x_6, y=y_6, text=y_6,
                                 textfont_size=18, textposition='auto')])
    fig.update_xaxes(title_text='path length', titlefont_size=18, dtick=1)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(
            title_text='Distribution of AS path length without prepending',
            title_font_size=18, xaxis_tickfont_size=18, yaxis_tickfont_size=18)
    plot(fig, auto_open=True)


# Graph AS distribution over RIRs
def graph_AS_per_RIR(hijack_dataset):
    """Make graphs for AS distribution over RIR

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    detected = hijack_dataset['detected ASN status']
    expected = hijack_dataset['expected ASN status']

    detected_RIRs = []
    expected_RIRs = []

    # Distribution of detected ASes
    for i in detected:
        detected_RIRs.append(i[0])
    detected_count = Counter(detected_RIRs)
    det_x = list(detected_count.keys())
    det_y = list(detected_count.values())

    # Distribution of ASes over RIRs
    for i in expected:
        expected_RIRs.append(i[0])
    expected_count = Counter(expected_RIRs)
    exp_x = list(expected_count.keys())
    exp_y = list(expected_count.values())

    fig = go.Figure(data=[go.Bar(name='Detected AS', x=det_x, y=det_y,
                                 text=det_y, textfont_size=18,
                                 textposition='auto'),
                          go.Bar(name='Expected AS', x=exp_x, y=exp_y,
                                 text=exp_y, textfont_size=18,
                                 textposition='auto')])
    fig.update_xaxes(title_text='RIR', titlefont_size=18)
    fig.update_yaxes(title_text='count', titlefont_size=18)
    fig.update_layout(title_text='Distribution of ASes over RIRs in hijacks',
                      title_font_size=18, xaxis_tickfont_size=18,
                      yaxis_tickfont_size=18, legend_font_size=16)
    plot(fig, auto_open=True)

    # Distribution of ASes globally
    glob_x = ['apnic', 'ripencc', 'arin', 'lacnic', 'afrinic']
    glob_y = [9323, 39447, 27222, 9190, 2302]

    fig = go.Figure(data=[go.Bar(name='Detected AS', x=glob_x, y=glob_y,
                                 text=glob_y, textfont_size=18,
                                 textposition='auto')])
    fig.update_xaxes(title_text='RIR', titlefont_size=18)
    fig.update_yaxes(title_text='count', titlefont_size=18)
    fig.update_layout(title_text='Distribution of ASes over RIRs globally',
                      title_font_size=18, xaxis_tickfont_size=18,
                      yaxis_tickfont_size=18, legend_font_size=16)
    plot(fig, auto_open=True)

    # Table detected vs expected per RIR
    RIRs = {'IANA': 0, 'afrinic': 0, 'apnic': 0,
            'arin': 0, 'lacnic': 0, 'ripencc': 0}
    count_pairs = {'IANA': RIRs.copy(), 'afrinic': RIRs.copy(),
                   'apnic': RIRs.copy(), 'arin': RIRs.copy(),
                   'lacnic': RIRs.copy(), 'ripencc': RIRs.copy()}

    for i in range(0, len(detected_RIRs)):
        det = detected_RIRs[i]
        exp = expected_RIRs[i]
        count_pairs[det][exp] = count_pairs[det][exp] + 1

    rir = ['IANA', 'afrinic', 'apnic', 'arin', 'lacnic', 'ripencc']
    values = [list(count_pairs['IANA'].values()),
              list(count_pairs['afrinic'].values()),
              list(count_pairs['apnic'].values()),
              list(count_pairs['arin'].values()),
              list(count_pairs['lacnic'].values()),
              list(count_pairs['ripencc'].values())]

    # Create LaTex table
    print('Detected AS | Expected AS & ' + ' & '.join(rir) +
          ' & Total \\\\ \\hline')
    for i in range(0, len(rir)):
        print(rir[i] + ' & ' + ' & '.join(map(str, values[i])) +
              ' & ' + str(sum(values[i])) + ' \\\\')


# Continuous and valley-free paths
def get_continuous_valley_paths(hijack_dataset):
    """Get statistics on continuous and valley-free paths

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    # Total
    continuous = Counter(hijack_dataset['continuous AS path'])
    valley = Counter(hijack_dataset['valley free path'])
    total = len(hijack_dataset)
    continuous = continuous[1.0]
    valley = valley[1.0]

    print('There are ' + str(continuous) + ' continuous paths. This is ' +
          str(round(continuous/total*100, 2)) + '%. From these paths ' +
          str(valley) + ' paths are valley-free. This is ' +
          str(round(valley/total*100, 2)) +
          '% of all paths and ' + str(round(valley/continuous*100, 2)) +
          '% of continuous paths.')

    # IPv4
    ipv4 = hijack_dataset[hijack_dataset['IP version'] == 4]
    continuous = Counter(ipv4['continuous AS path'])
    valley = Counter(ipv4['valley free path'])
    total = len(ipv4)
    continuous = continuous[1.0]
    valley = valley[1.0]

    print('In the IPv4 hijacks there are ' + str(continuous) +
          ' continuous paths. This is ' + str(round(continuous/total*100, 2)) +
          '%. From these paths ' + str(valley) +
          ' paths are valley-free. This is ' +
          str(round(valley/total*100, 2)) +
          '% of all paths and ' + str(round(valley/continuous*100, 2)) +
          '% of continuous paths.')

    # IPv6
    ipv6 = hijack_dataset[hijack_dataset['IP version'] == 6]
    continuous = Counter(ipv6['continuous AS path'])
    valley = Counter(ipv6['valley free path'])
    total = len(ipv6)
    continuous = continuous[1.0]
    valley = valley[1.0]

    print('In the IPv6 hijacks there are ' + str(continuous) +
          ' continuous paths. This is ' + str(round(continuous/total*100, 2)) +
          '%. From these paths ' + str(valley) +
          ' paths are valley-free. This is ' +
          str(round(valley/total*100, 2)) +
          '% of all paths and ' + str(round(valley/continuous*100, 2)) +
          '% of continuous paths.')


# Results on customer cone size and BGP mon peers
def graph_cc_size_and_bgpmon_peers(hijack_dataset):
    """Find if there is a relation between cc size and number of
    BGPMon peers that detected the hijack

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    det_size = hijack_dataset['size customer cone detected ASN']
    exp_size = hijack_dataset['size customer cone expected ASN']

    # Number of BGPMon peers distribution
    nbins = int(max(hijack_dataset['detected by x BGPMon peers']))
    peers = hijack_dataset[['detected by x BGPMon peers',
                            'detected advertisement subnet of expected',
                            'size customer cone detected ASN']]
    global_hijacks = peers[peers[
            'detected advertisement subnet of expected'] == 1]
    local_hijacks = peers[peers[
            'detected advertisement subnet of expected'] == 0]

    glob = list(global_hijacks['detected by x BGPMon peers'])
    loc = list(local_hijacks['detected by x BGPMon peers'])

    fig = go.Figure()
    fig.add_trace(go.Histogram(x=glob, name='Global hijack',
                  nbinsx=nbins))
    fig.add_trace(go.Histogram(x=loc, name='Local hijack',
                  nbinsx=nbins))
    fig.update_xaxes(title_text='Number of BGPMon peers', titlefont_size=18,
                     rangemode='tozero')
    fig.update_yaxes(title_text='count', titlefont_size=18)
    fig.update_layout(title_text='Distribution number of BGPMon peers ' +
                      'that received hijack route', barmode='overlay',
                      title_font_size=18, xaxis_tickfont_size=18,
                      yaxis_tickfont_size=18)
    fig.update_traces(opacity=0.55)
    plot(fig, auto_open=True)

    # BGPMON peers vs detected AS size
    glob_x = list(global_hijacks['size customer cone detected ASN'])
    loc_x = list(local_hijacks['size customer cone detected ASN'])
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=glob_x, y=glob, mode='markers',
                             name='Global hijack'))
    fig.add_trace(go.Scatter(x=loc_x, y=loc, mode='markers',
                             name='Local hijack'))
    fig.update_xaxes(dtick=1000, rangemode='tozero',
                     title_text='size of customer cone of detected AS',
                     titlefont_size=18)
    fig.update_yaxes(title_text='number of BGPMon peers', titlefont_size=18)
    fig.update_layout(title_text='BGPMon peers and detected AS size',
                      title_font_size=18, xaxis_tickfont_size=18,
                      xaxis_title_font_size=18, yaxis_title_font_size=18,
                      yaxis_tickfont_size=18)
    fig.update_traces(opacity=0.55)
    plot(fig, auto_open=True)

    # AS size distribution
    all_cc_data_2018 = data.AS_relationship_data['20180501.ppdc-ases.txt']
    all_cc_data_2019 = data.AS_relationship_data['20190501.ppdc-ases.txt']
    all_sizes_2018 = []
    all_sizes_2019 = []
    for i in range(0, len(all_cc_data_2018)):
        cc = all_cc_data_2018.iloc[i]['customers'].split(' ')
        all_sizes_2018.append(len(cc)-1)

    for i in range(0, len(all_cc_data_2019)):
        cc = all_cc_data_2019.iloc[i]['customers'].split(' ')
        all_sizes_2019.append(len(cc)-1)

    fig = go.Figure()
    fig.add_trace(go.Histogram(x=list(det_size), name='Detected AS'))
    fig.add_trace(go.Histogram(x=list(exp_size), name='Expected AS'))
    fig.update_xaxes(title_text='AS customer cone size', titlefont_size=18)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(
            barmode='overlay',
            title_text='Distribution of AS customer cone size in hijacks',
            title_font_size=18, xaxis_tickfont_size=18,
            yaxis_tickfont_size=18, legend_font_size=17)
    fig.update_traces(opacity=0.65)
    plot(fig, auto_open=True)

    # Graph all AS
    fig = go.Figure(
            [go.Histogram(name='May 2018', x=all_sizes_2018, nbinsx=80),
             go.Histogram(name='May 2019', x=all_sizes_2019, nbinsx=80)])
    fig.update_xaxes(title_text='AS customer cone size', titlefont_size=18)
    fig.update_yaxes(title_text='count', titlefont_size=18, type='log')
    fig.update_layout(
            barmode='overlay',
            title_text='Distribution of AS customer cone size globally',
            title_font_size=18, xaxis_tickfont_size=18,
            xaxis_title_font_size=18, yaxis_title_font_size=18,
            yaxis_tickfont_size=18)
    fig.update_traces(opacity=0.65)
    plot(fig, auto_open=True)


# Graph path length and BGPMon peers
def graph_path_length_bgpmon(hijack_dataset):
    """Find if there is a relation between AS path length and number of
    BGPMon peers that detected the hijack

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    peers = hijack_dataset[['detected by x BGPMon peers',
                            'detected advertisement subnet of expected',
                            'detected AS Path']]
    global_hijacks = peers[peers[
            'detected advertisement subnet of expected'] == 1]
    local_hijacks = peers[peers[
            'detected advertisement subnet of expected'] == 0]

    # BGPMON peers vs AS path length unique ASes
    glob = list(global_hijacks['detected by x BGPMon peers'])
    loc = list(local_hijacks['detected by x BGPMon peers'])
    glob_x = []
    loc_x = []

    for p in global_hijacks['detected AS Path']:
        p = list(dict.fromkeys(p.split(' ')))  # remove duplicates
        glob_x.append(len(p))

    for p in local_hijacks['detected AS Path']:
        p = list(dict.fromkeys(p.split(' ')))  # remove duplicates
        loc_x.append(len(p))

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=glob_x, y=glob, mode='markers',
                             name='Global hijack'))
    fig.add_trace(go.Scatter(x=loc_x, y=loc, mode='markers',
                             name='Local hijack'))
    fig.update_xaxes(rangemode='tozero',
                     title_text='number of unique ASNs in AS path',
                     titlefont_size=18, dtick=1)
    fig.update_yaxes(title_text='number of BGPMon peers', titlefont_size=18)
    fig.update_layout(
            title_text='BGPMon peers and number of unique ASNs in AS path',
            title_font_size=18, xaxis_tickfont_size=18,
            xaxis_title_font_size=18, yaxis_title_font_size=18,
            yaxis_tickfont_size=18)
    fig.update_traces(opacity=0.55)
    plot(fig, auto_open=True)


# Graph AS global picture
def graph_AS_global_picture(hijack_dataset):
    """Get a global map of detected ASes and expected ASes

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    # AS countries
    det_country = hijack_dataset['country detected ASN']
    exp_country = hijack_dataset['country expected ASN']

    file = config.data_directory.joinpath('cc_asn.txt')
    glob_country = pd.read_csv(file, sep=',')

    # Get most common countries
    det = []
    exp = []
    for i in det_country:
        c = mpu.datastructures.flatten(i)
        if c[0] is None and c[2] is None:
            if c[1] != '' and type(c[1]) != float:
                det.append(c[1])
        else:
            most_common = Counter(c).most_common()
            if len(most_common) == 1:
                det.append(most_common[0][0])
            elif most_common[0][1] > most_common[1][1]:
                det.append(most_common[0][0])
            else:
                if most_common[0][0] is None:
                    det.append(most_common[1][0])
                else:
                    det.append(most_common[0][0])

    for i in exp_country:
        c = mpu.datastructures.flatten(i)
        if c[0] is None and c[2] is None:
            if c[1] != '':
                if c[1] != '' and type(c[1]) != float:
                    exp.append(c[1])
        else:
            most_common = Counter(c).most_common()
            if len(most_common) == 1:
                exp.append(most_common[0][0])
            elif most_common[0][1] > most_common[1][1]:
                exp.append(most_common[0][0])
            else:
                if most_common[0][0] is None:
                    exp.append(most_common[1][0])
                else:
                    exp.append(most_common[0][0])

    # Convert from alpha-2 to alpha-3
    det3 = []
    exp3 = []

    for i in det:
        c = pc.countries.get(alpha_2=i)
        if c is not None:
            det3.append(c.alpha_3)

    for i in exp:
        c = pc.countries.get(alpha_2=i)
        if c is not None:
            exp3.append(c.alpha_3)

    # Detected AS countries
    det_x = list(Counter(det3).keys())
    det_y = list(Counter(det3).values())
    norm_y = []

    # Normalize data
    for i in range(0, len(det_x)):
        cc2 = pc.countries.get(alpha_3=det_x[i]).alpha_2
        glob = glob_country[glob_country['CC'] == cc2]
        norm_y.append(round(det_y[i]/glob.iloc[0][1], 4))

    fig = go.Figure(data=go.Choropleth(locations=det_x, z=det_y,
                                       colorscale='Reds', autocolorscale=False,
                                       reversescale=False,
                                       marker_line_color='darkgray',
                                       marker_line_width=0.5,
                                       colorbar_title='Count',
                                       colorbar_title_font_size=18,
                                       colorbar_tickfont_size=18))
    fig.update_layout(title_text='Number of detected ASes per country',
                      title_font_size=18)
    plot(fig, auto_open=True)

    fig = go.Figure(data=go.Choropleth(locations=det_x, z=norm_y,
                                       colorscale='Reds', autocolorscale=False,
                                       reversescale=False,
                                       marker_line_color='darkgray',
                                       marker_line_width=0.5,
                                       colorbar_title='Count',
                                       colorbar_title_font_size=18,
                                       colorbar_tickfont_size=18))
    fig.update_layout(title_text='Detected ASes per country normalised',
                      title_font_size=18)
    plot(fig, auto_open=True)

    # Expected AS countries
    exp_x = list(Counter(exp3).keys())
    exp_y = list(Counter(exp3).values())
    norm_y = []

    # Normalize data
    for i in range(0, len(exp_x)):
        cc2 = pc.countries.get(alpha_3=exp_x[i]).alpha_2
        glob = glob_country[glob_country['CC'] == cc2]
        norm_y.append(round(exp_y[i]/glob.iloc[0][1], 4))

    fig = go.Figure(data=go.Choropleth(locations=exp_x, z=exp_y,
                                       colorscale='Reds', autocolorscale=False,
                                       reversescale=False,
                                       marker_line_color='darkgray',
                                       marker_line_width=0.5,
                                       colorbar_title='Count',
                                       colorbar_title_font_size=18,
                                       colorbar_tickfont_size=18))
    fig.update_layout(title_text='Number of expected ASes per country',
                      title_font_size=18)
    plot(fig, auto_open=True)

    fig = go.Figure(data=go.Choropleth(locations=exp_x, z=norm_y,
                                       colorscale='Reds', autocolorscale=False,
                                       reversescale=False,
                                       marker_line_color='darkgray',
                                       marker_line_width=0.5,
                                       colorbar_title='Count',
                                       colorbar_title_font_size=18,
                                       colorbar_tickfont_size=18))
    fig.update_layout(title_text='Expected ASes per country normalised',
                      title_font_size=18)
    plot(fig, auto_open=True)


# Get statistics on hijack duration
def get_hijack_duration_stats(hijack_dataset):
    """Get statistics on hijack duration

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    zero_second = hijack_dataset[hijack_dataset['hijack duration'] == (0, 0)]
    longer = hijack_dataset[hijack_dataset['hijack duration'] != (0, 0)]

    zero = len(zero_second)
    p_zero = round(zero/len(hijack_dataset)*100, 2)
    print(str(zero) + ' (' + str(p_zero) + '%) hijacks last 0 seconds. ' +
          str(len(longer)) + ' hijacks last longer than that.')

    duration = longer['hijack duration']
    prefix_max = []
    prefix_zero = []
    as_max = []
    as_zero = []
    equal = []
    max_per_hijack = []

    for i in duration:
        if i[0] > i[1]:
            prefix_max.append(i)
            max_per_hijack.append(i[0])
            if i[1] == 0:
                as_zero.append(i)
        elif i[1] > i[0]:
            as_max.append(i)
            max_per_hijack.append(i[1])
            if i[0] == 0:
                prefix_zero.append(i)
        else:
            equal.append(i)
            max_per_hijack.append(i[0])

    print('There are ' + str(len(equal)) + ' hijacks that have an equal ' +
          'duration in prefix history and AS history. For ' +
          str(len(prefix_max)) + ' hijacks the prefix history has the ' +
          ' longest duration. For ' + str(len(as_max)) + ' hijacks the AS ' +
          'history has the longest duration. The maximum duration is ' +
          str(max(max_per_hijack)) + ' seconds. The minimum larger than 0 ' +
          ' is ' + str(min(max_per_hijack)) + ' seconds.')

    # Graph durations
    days = [round(x/86400, 1) for x in max_per_hijack]  # seconds to days
    nbins = len(Counter(days))

    fig = go.Figure(data=[go.Histogram(x=days, nbinsx=nbins)])
    fig.update_xaxes(title_text='duration in days', dtick=10,
                     titlefont_size=18, rangemode='tozero')
    fig.update_yaxes(title_text='count', titlefont_size=18)
    fig.update_layout(title_text='Distribution of hijack duration in days ' +
                      'for hijacks that lasted at least one day',
                      title_font_size=18, xaxis_tickfont_size=18,
                      xaxis_title_font_size=18, yaxis_title_font_size=18,
                      yaxis_tickfont_size=18)
    plot(fig, auto_open=True)


# Relation detected AS and expected AS
def get_relation_hijack_ASes(hijack_dataset):
    """Get statistics on the relation between the detected AS and expected AS
    in each hijack.

    Parameters:
        hijack_dataset(DataFrame): hijack dataset generated in main
    """

    no_direct_links = hijack_dataset[
            (hijack_dataset['relation detected-expected'] == 'no relation') &
            (hijack_dataset['relation detected-expected nm'] == 'no relation')]
    p_no = round(len(no_direct_links)/len(hijack_dataset)*100, 2)
    cc_rel = no_direct_links[
            (no_direct_links['det in customer cone of exp'] == True) |
            (no_direct_links['det in customer cone of exp nm'] == True) |
            (no_direct_links['exp in customer cone of det'] == True) |
            (no_direct_links['exp in customer cone of det nm'] == True)]
    p_cc_no = round(len(cc_rel)/len(hijack_dataset)*100, 2)

    print('There are ' + str(len(no_direct_links)) + ' hijacks without a ' +
          'direct relation between the detected and expected AS. This is ' +
          str(p_no) + '% of the cases. From these hijacks ' +
          str(len(cc_rel)) + ' are related by customer cone. This is ' +
          str(p_cc_no) + '% of all hijacks. Meaning that ' +
          str(p_no-p_cc_no) + '% of hijacks have no relation between the ' +
          'detected and expected AS.')

    direct_links = hijack_dataset[
            (hijack_dataset['relation detected-expected'] != 'no relation') |
            (hijack_dataset['relation detected-expected nm'] != 'no relation')]
    p_rel = round(len(direct_links)/len(hijack_dataset)*100, 2)
    p2p = direct_links[
            (direct_links['relation detected-expected'] == 'p2p') |
            (direct_links['relation detected-expected nm'] == 'p2p')]
    p_p2p = round(len(p2p)/len(hijack_dataset)*100, 2)
    other = direct_links[
            (direct_links['relation detected-expected'] == 'p2c') |
            (direct_links['relation detected-expected nm'] == 'p2c') |
            (direct_links['relation detected-expected'] == 'c2p') |
            (direct_links['relation detected-expected nm'] == 'c2p')]
    p_other = round(len(other)/len(hijack_dataset)*100, 2)

    print('There are ' + str(len(direct_links)) + ' hijacks with a ' +
          'direct relation between the detected and expected AS. This is ' +
          str(p_rel) + '% of the cases. Of these hijacks there are ' +
          str(len(p2p)) + ' hijacks with a p2p relation. This is ' +
          str(p_p2p) + '%. The other ' + str(len(other)) + ' hijacks have ' +
          ' a p2c or c2p relation. This is ' + str(p_other) + '%.')
